mkdir build
cd parser
scons

rm config.log
rm .sconsign.dblite
rm cinnamon.exp
rm cinnamon.lib
rm -rf .sconf_temp
rm -rf build

cd ..
mv -f parser/cinnamon build
mv -f parser/cinnamon.exe build