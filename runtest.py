
"""
env.transpile(cinnamonFile, expectedErrors = [], findInHeader = [], findInCpp = [])
env.compile(expectedErrors = [])
env.run(self, arguments = "", expectedOutputs = [], unwantedOutputs = [], expectedReturn = 0)
"""

PRECOMPILED_HEADERS = True

import subprocess
import re
import os
import sys
import time
import traceback

def gcc(sources, output, includes=[]):
	global phdir

	return subprocess.run(
			("g++ -DMARK=m0n -o " + output + " -std=c++17 -Wall -Wextra -Wpedantic -pthread -g -I"
			+ phdir + " -Ilibs " + " ".join(["-I" + inc for inc in includes] )).split() + sources,
			stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

def getOutput(result):
	return result.stdout.decode("utf-8").replace("\r\n", "\n")

class TestEnv:
	def __init__(self, srcdir, builddir, verbose):
		self.srcdir = srcdir
		self.builddir = builddir
		self.verbose = verbose
		self.problems = 0
		self.plugins = []
		self.h = None
		self.cpp = None
		self.exe = None
		self.transpiled = False
		self.compiled = False

	@classmethod
	def print(self, msg):
		print("    " + msg)

	def problem(self, msg):
		print("<!> " + msg)
		self.problems = self.problems + 1

	@classmethod
	def blockprint(self, msg):
		print("     " + ("_"*75))
		print("    |" + msg.replace("\n", "\n    |   "))
		print("    |" + ("_"*75) + "\n")

	def line(self):
		print("    " + "~ "*38)

	def crossMatch(self, occuredErrors, expectedErrors):
		check = [False] * len(occuredErrors)
		for exp in expectedErrors:
			found = False
			for i in range(len(occuredErrors)):
				if occuredErrors[i].find(exp) > -1:
					check[i] = True
					found = True
					self.print("As expected: " + exp)
					break
			if not found:
				self.problem("Did not occur: " + exp)
		for i in range(len(check)):
			if not check[i]:
				self.problem("Unexpected: " + occuredErrors[i])

	def transpile(self, cinnamonFile, plugins = [], expectedErrors = [], findInHeader = [], findInCpp = []):

		self.print("Transpiling...")
		self.print("  " + os.path.join(self.srcdir, cinnamonFile))

		self.transpiled = False
		self.compiled = False
		self.plugins = plugins

		self.exe = cinnamonFile.replace(".", "_")

		verboseflag = "--verbose" if self.verbose else ""

		result = subprocess.run(
				("build/cinnamon cinnamon.lua -b " + self.builddir + " " + verboseflag +
				" " + os.path.join(self.srcdir, cinnamonFile) + " " +
				' '.join(["-p " + p for p in plugins])).split(),
				stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		output = "\n" + getOutput(result)

		if self.verbose:
			self.blockprint(output)

		scriptErrors = re.findall(r"\ntranspilation error: (.*)", output)
		transpilerErrors = re.findall(r"\nerror executing (.*)", output)

		self.crossMatch(scriptErrors, expectedErrors)

		for te in transpilerErrors: # errors in the transpiler itself should never happen
			self.problem("Error in transpiler: " + te)

		self.h = re.findall(r"\nwriting header file (.*)", output)
		self.cpp = re.findall(r"\nwriting source file (.*)", output)

		allh = ""
		for hf in self.h:
			with open(hf, encoding='utf-8') as f:
				allh = allh + f.read()
		allcpp = ""
		for cf in self.cpp:
			with open(cf, encoding='utf-8') as f:
				allcpp = allcpp + f.read()

		for fih in findInHeader:
			if allh.find(fih) == -1:
				self.problem("Not found in header files: " + fih)
			else:
				self.print("Found in header files: " + fih)

		for fic in findInCpp:
			if allcpp.find(fic) == -1:
				self.problem("Not found in cpp files: " + fic)
			else:
				self.print("Found in cpp files: " + fic)

		if len(expectedErrors) == 0 and result.returncode != 0:
			self.problem("Transpilation failed, return code is " + str(result.returncode))

		if result.returncode == 0:
			self.transpiled = True

		return result.returncode


	def compile(self, expectedErrors = []):

		self.compiled = False

		if not self.transpiled:
			self.problem("Transpiling was not successful, will not compile.")
			return 1

		if False and PRECOMPILED_HEADERS:
			self.print("Renaming std.hpp to std.hpp_ so that precompiled headers are used")
			try:
				os.remove(os.path.join(builddir, "std.hpp_"))
			except:
				pass
			try:
				os.rename(os.path.join(builddir, "std.hpp"), os.path.join(builddir, "std.hpp_"))
			except:
				pass


		self.print("Compiling...")
		for f in self.cpp:
			self.print("  " + f)

		result = gcc(self.cpp, os.path.join(self.builddir, self.exe), ["plugins/" + p + "/include" for p in self.plugins])
		output = "\n" + getOutput(result)
		output = output.replace("‘", "'").replace("’", "'")

		if self.verbose:
			self.blockprint(output)

		fatals = re.findall(r"\n(.*?: fatal error: .*)", output)
		nonfatals = re.findall(r"\n(.*?: error: .*)", output)
		warnings = re.findall(r"\n(.*?: warning: .*)", output)
		linker = re.findall(r"\n(.*?: undefined reference to .*)", output)

		errors = fatals + nonfatals + linker

		self.crossMatch(errors, expectedErrors)

		if len(errors) == 0 and result.returncode != 0:
			self.problem("Compilation failed unexpectedly (and the error wasn't even listed!)")

		if result.returncode == 0:
			self.compiled = True

		return result.returncode


	def run(self, arguments = "", expectedOutputs = [], unwantedOutputs = [], expectedReturn = 0):

		if not self.compiled:
			self.problem("Compiling was not successful, will not run.")
			return 1

		self.print("Running...")
		self.print("  " + os.path.join(self.builddir, self.exe))

		result = subprocess.run(
				(os.path.join(self.builddir, self.exe) + " " + arguments).split(),
				stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		output = "\n" + getOutput(result)

		if self.verbose:
			self.blockprint(output)

		for exp in expectedOutputs:
			if output.find(exp) == -1:
				self.problem("Not found in program output: " + exp)
			else:
				self.print("Found in program output: " + exp)

		for dont in unwantedOutputs:
			if output.find(dont) > -1:
				self.problem("Unwanted occurance in program output: " + dont)
			else:
				self.print("Fortunately not found in program output: " + dont)

		if result.returncode != expectedReturn and expectedReturn != None:
			self.problem("Unexpected return value: " + str(result.returncode))

		return result.returncode


t0 = time.time()
problems = 0
dirs = os.listdir("tests")
dirs = [dir for dir in dirs if not os.path.isfile(dir)]
dirs.sort()
specific = len(sys.argv) > 1

phdir = "."
if PRECOMPILED_HEADERS:
	phdir = os.path.join("build", "precompiled_headers")
	if specific:
		TestEnv.print("using precompiled headers without rebuilding them...")
	else:
		TestEnv.print("precompiling headers...")
		os.makedirs(phdir, exist_ok=True)
		result = gcc(["libs/std.hpp"], os.path.join(phdir, "std.hpp.gch"))
		if result.returncode == 0:
			TestEnv.print("ok")
		else:
			TestEnv.print("failed:")
			TestEnv.blockprint(result.stdout.decode("utf-8"))

for dir in dirs:
	srcdir = os.path.join("tests", dir)

	if specific and srcdir.find(sys.argv[1]) == -1: # filter this test out
		continue

	if not specific and dir == "sandbox":
		continue

	print("\n\n\n    " + dir)
	print("    " + "=" * len(dir))

	builddir = os.path.join("build", srcdir)
	os.makedirs(builddir, exist_ok=True)

	env = TestEnv(srcdir, builddir, verbose=specific)

	testscript = os.path.join(srcdir, "test.py")
	if os.path.isfile(testscript):
		with open(testscript, encoding='utf-8') as f:
			code = compile(f.read(), testscript, 'exec')
			exec(code, {"env":env})

	elif os.path.isfile(os.path.join(srcdir, "main.mon")):
		env.transpile("main.mon")
		env.compile()
		retn = env.run()
		if retn == 0:
			env.print("Success!")

	else:
		env.problem('No "test.py" script and no "main.mon" in ' + srcdir)

	problems = problems + env.problems


report = str(problems) + " problem"
if problems != 1:
	report = report + "s"

report = report + "; " + str(int((time.time() - t0)*100)/100) + " s"

print("\n" + "_" * (len(report)+4) + "\n    " + report + "\n")
