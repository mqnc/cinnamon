
--print(_VERSION)

local scriptPath = string.gsub(arg[0], "cinnamon.lua$", "")

utils = require (scriptPath .. "tools/utils")
log = utils.log
col = utils.colorize
stringify = utils.stringify
dump = function(arg) print(stringify(arg)) end
fields = utils.fields

local argparse = require (scriptPath .. "tools/argparse")
local argparser = argparse("cinnamon cinnamon.lua", "cinnamon cinnamon.lua -b buildfolder srcfolder/main.mon")
argparser:argument("input", "main module file"):args("?")
argparser:option("-b --build", "build directory", ".")
argparser:option("-p --peg", "store peg grammar in file")
argparser:option("-m --mark", "mark for cinnamon keywords", "m0n")
argparser:option("-p --plugin", "load language plugin"):count("*")
argparser:flag("-g --debug", "display debug information")
argparser:flag("-s --scons", "generate SConstruct file")
argparser:flag("-v --verbose", "display generated C++ source")
args = argparser:parse()
mark = args.mark

if args.build ~= "." then
	lfs.mkdir(args.build)
end

transpiler = require (scriptPath .. "tools/transpiler")()
rule = transpiler.rule
basic = transpiler.basicActions

errors = 0
function transpilerError(msg)
	print("transpilation error: " .. msg)
	errors = errors+1
end

-- the language modules add elements to these tables and they will be turned into grammar in the end
keywords = {}
atomics = {}
globalStatements = {}
localStatements = {}

-- load the language modules
local sep = utils.pathSep -- platform specific path seperator
dofile(scriptPath .. "grammar" .. sep .. "core.lua")
dofile(scriptPath .. "grammar" .. sep .. "chain.lua")
dofile(scriptPath .. "grammar" .. sep .. "literal.lua")
dofile(scriptPath .. "grammar" .. sep .. "operator.lua")
dofile(scriptPath .. "grammar" .. sep .. "variable_generator.lua")
dofile(scriptPath .. "grammar" .. sep .. "declaration.lua")
dofile(scriptPath .. "grammar" .. sep .. "function_generator.lua")
dofile(scriptPath .. "grammar" .. sep .. "function.lua")
dofile(scriptPath .. "grammar" .. sep .. "parameter.lua")
dofile(scriptPath .. "grammar" .. sep .. "branch.lua")
dofile(scriptPath .. "grammar" .. sep .. "range.lua")
dofile(scriptPath .. "grammar" .. sep .. "loop.lua")
dofile(scriptPath .. "grammar" .. sep .. "field_generator.lua")
dofile(scriptPath .. "grammar" .. sep .. "class_generator.lua")
dofile(scriptPath .. "grammar" .. sep .. "class.lua")
dofile(scriptPath .. "grammar" .. sep .. "import.lua")
dofile(scriptPath .. "grammar" .. sep .. "rawcpp.lua")
dofile(scriptPath .. "grammar" .. sep .. "scope.lua")

for i=1, #args.plugin do
	dofile(scriptPath .. "plugins" .. sep .. args.plugin[i] .. sep .. "plugin.lua")
end

-- these elements need to be the last ones that are tested
table.insert(localStatements, "Expression _ Terminal")
table.insert(globalStatements, "SyntaxError")
table.insert(localStatements, "SyntaxError")

-- construct expression grammar from operators
dofile(scriptPath .. "grammar" .. sep .. "expression.lua")

-- construct grammar from the tables
rule( " Keyword <- (" .. table.concat(keywords, " / ") .. " ) WordEnd", basic.forward(1))
rule( " Atomic <- " .. table.concat(atomics, " / "), basic.forward(1))
rule( " GlobalStatement <- " .. table.concat(globalStatements, " / "), basic.concat)
rule( " LocalStatement <- " .. table.concat(localStatements, " / "), basic.concat)

if args.peg then
	-- store grammar
	local target = args.peg
	if args.build then
		target = args.build .. "/" .. args.peg
	end
	print("writing grammar file " .. target)
	utils.writeToFile(target, transpiler.grammar())
end

if args.input then
	local inputFolder, mainFile, inputExtension = utils.splitFile(args.input)

	fileQueue = {}
	table.insert(fileQueue, mainFile)

	local currentFile = 1
	while fileQueue[currentFile] do

		local inputFile = fileQueue[currentFile]

		print("transpiling " .. inputFolder .. inputFile .. inputExtension .. "...")

		local input = utils.readAll(inputFolder .. inputFile .. inputExtension)

		-- transpile
		namespace = inputFile
		modules = {}

		codeSections = {}

		codeSections.headerHeader = ""
		codeSections.headerIncludes = '#include "cinnamon.hpp"\n'
		for i=1, #args.plugin do
			codeSections.headerIncludes = codeSections.headerIncludes .. '#include "' .. args.plugin[i] .. '.hpp"\n'
		end
		codeSections.headerUsingDeclarations = "using namespace cinnamon;\n"
		codeSections.headerClassForwardDeclarations = ""
		codeSections.headerFunctionDeclarations = ""
		codeSections.headerClassDeclarations = ""
		codeSections.headerFunctionDefinitions = ""
		codeSections.headerFooter = ""

		codeSections.sourceHeader = ""
		codeSections.sourceIncludes = '#include "' .. inputFile .. '.hpp"\n'
		codeSections.sourceUsingDeclarations = ""
		codeSections.sourceClassForwardDeclarations = ""
		codeSections.sourceFunctionDeclarations = ""
		codeSections.sourceClassDeclarations = ""
		codeSections.sourceFunctionDefinitions = ""
		codeSections.sourceFooter = ""

		functionStack = {} -- doesn't actually need to be a stack at this point as functions cant be nested yet
		classStack = {} -- same here
		scopeStack = {}

		local t0 = os.clock()
		-- local result = transpiler.transpile(input, args.debug).txt
		transpiler.transpile(input, args.debug)
		local t1 = os.clock()

		-- prettify
		--transpiler.clear()
		local prettify = require (scriptPath .. "tools/prettify")

		local sha2 = require (scriptPath .. "tools/sha2")

		local header = prettify(
			codeSections.headerHeader .. "\n" ..
			codeSections.headerIncludes .. "\n" ..
			"namespace " .. namespace .. "_" .. mark .. "_ns{\n" ..
			codeSections.headerUsingDeclarations .. "\n" ..
			codeSections.headerClassForwardDeclarations .. "\n" ..
			codeSections.headerFunctionDeclarations .. "\n" ..
			codeSections.headerClassDeclarations .. "\n" ..
			codeSections.headerFunctionDefinitions .. "\n" ..
			"}//namespace\n" ..
			codeSections.headerFooter .. "\n"
		)

		local hash = "SHA256_" .. string.upper(sha2.hash256(inputFile .. input)) .. "_HPP"

		header =
			"#ifndef " .. hash .. "\n" ..
			"#define " .. hash .. "\n" ..
			header ..
	 		"\n#endif\n"

		local source = prettify(
			'#include "std.hpp"\n' ..
			codeSections.sourceHeader .. "\n" ..
			codeSections.sourceIncludes .. "\n" ..
			"namespace " .. namespace .. "_" .. mark .. "_ns{\n" ..
			codeSections.sourceUsingDeclarations .. "\n" ..
			codeSections.sourceClassForwardDeclarations .. "\n" ..
			codeSections.sourceFunctionDeclarations .. "\n" ..
			codeSections.sourceClassDeclarations .. "\n" ..
			codeSections.sourceFunctionDefinitions .. "\n" ..
			"}//namespace\n" ..
			codeSections.sourceFooter .. "\n"
		)

		if args.verbose then
			-- display results
			print("\n" .. inputFile .. ".hpp")
			print(string.rep("=", #inputFile+4))
			print(header)
			print("\n" .. inputFile .. ".cpp")
			print(string.rep("=", #inputFile+4))
			print(source)
			print("transpile CPU time: " .. t1-t0 .. "s")
		end

		print("writing header file " .. args.build .. "/" .. inputFile .. ".hpp")
		utils.writeToFile(args.build .. "/" .. inputFile .. ".hpp", header)
		print("writing source file " .. args.build .. "/" .. inputFile .. ".cpp")
		utils.writeToFile(args.build .. "/" .. inputFile .. ".cpp", source)

		currentFile = currentFile + 1
	end

	--[[
	for file in lfs.dir(scriptPath .. "libs") do
		if file:sub(-4) == ".hpp" then
			local code = utils.readAll(scriptPath .. "libs/" .. file)
			code = string.gsub(code, "##MARK##", mark)
			code = string.gsub(code, "MARK##", mark)
			code = string.gsub(code, "##MARK", mark)
			print("writing include file " .. args.build .. "/" .. file)
			utils.writeToFile(args.build .. "/" .. file, code)
		end
	end
	]]

	if args.scons then
		scons = [[
env = Environment(TARGET_ARCH = 'x86')

if "msvc" in env["TOOLS"]:
	env.AppendUnique(CXXFLAGS=["/O2"])
	env.AppendUnique(CXXFLAGS=["/EHsc"])
else:
	env.AppendUnique(CCFLAGS=["-Wall", "-Wextra", "-g", "-pedantic", "-pthread", "-O2"])
	env.AppendUnique(CXXFLAGS=["-std=c++17"])
	env.AppendUnique(LINKFLAGS=["-pthread"])

env.Program("]] .. mainFile .. [[", ["]] .. table.concat(fileQueue, '.cpp", "') .. [[.cpp"])
]]

		print("writing scons file " .. args.build .. "/SConstruct")
		utils.writeToFile(args.build .. "/SConstruct", scons)
	end

end

if errors > 0 then
	os.exit(1)
end
