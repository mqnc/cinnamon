
rule([[ FunctionDeclaration <- FunctionKeyword _ Identifier _ TemplateParameters _ Parameters _ ReturnDeclaration _ FunctionSpecifiers _ Terminal FunctionBody EndFunctionKeyword ]], functionGenerator )

table.insert(globalStatements, "FunctionDeclaration")

rule([[ FunctionKeyword <- 'function' ]], function()
	-- push an empty slot to the function stack for the return statement to store its info in
	table.insert(functionStack, {})
	table.insert(scopeStack, {})
	return {}
end )
table.insert(keywords, "FunctionKeyword")

rule([[ FunctionSpecifiers <- FunctionSpecifierList / Empty ]], basic.choice("list", "none") )
rule([[ FunctionSpecifierList <- SpecifierIndicator _ SomeFunctionSpecifiers ]], basic.forward(3) )
rule([[ SomeFunctionSpecifiers <- FunctionSpecifier (_ Comma _ FunctionSpecifier)* ]], basic.listFilter)
rule([[ FunctionSpecifier <- FunctionInline / FunctionKwargs / FunctionExport ]], basic.match )
rule([[ FunctionInline <- "inline" ]])
rule([[ FunctionKwargs <- "kwargs" ]])
rule([[ FunctionExport <- "export" ]])
rule([[ SpecifierIndicator <- "|" ]])
table.insert(keywords, "FunctionInline")
table.insert(keywords, "FunctionKwargs")

rule([[ ReturnDeclaration <- ExplicitReturnDeclaration / DeduceReturnType ]], basic.choice("explicit", "deduce") )
rule([[ DeduceReturnType <- '' ]])
rule([[ ExplicitReturnDeclaration <- ReturnOperator _ ReturnType ]], basic.forward(3) )
rule([[ ReturnOperator <- '->' ]])

rule([[ ReturnType <- ReturnTuple / ReturnStruct / ScopedObjectOrSpecializedTypeOrFunction]], basic.subchoice("tuple", "struct", "single") )

-- tuple when two or more return values
rule([[ ReturnTuple <- SpecifiedType _ Comma _ SpecifiedType (_ Comma _ SpecifiedType)* ]], basic.listFilter )
rule([[ SpecifiedType <- ScopedObjectOrSpecializedTypeOrFunction ]], function(sv, info) return {name=sv[1].txt} end )

rule([[ ReturnStruct <- ReturnStructField _ Comma _ ReturnStructField (_ Comma _  ReturnStructField)* ]], basic.listFilter )

rule([[ ReturnStructField <- Identifier _ TypeDeclareOperator _ ScopedObjectOrSpecializedTypeOrFunction ]],
	function(sv, info) return {name=sv[1].txt, type=sv[5].txt} end )

rule([[ FunctionBody <- Skip (!EndKeyword LocalStatement Skip)* ]], basic.concat )

--rule([[ ReturnStatement <- ReturnKeyword _ Returnee _ Terminal ]], function(sv, info)
--	return {[1] = sv[1].txt .. sv[2].txt .. sv[3].txt .. sv[4].txt .. sv[5].txt, type = sv[3].rule}
--end )
rule([[ ReturnStatement <- ReturnKeyword _ Returnee _ Terminal ]], function(sv, info)
	-- store the returnee in the function stack so the function can access it
	functionStack[#functionStack] = sv[3]
	return basic.concat(sv, info)
end )
table.insert(localStatements, "ReturnStatement")

rule([[ Returnee <- Assigned / ReturnNothing ]], basic.choice("nonvoid", "void") )

rule([[ ReturnNothing <- '' ]], '')

rule([[ ReturnKeyword <- 'return' ]], 'return')

rule([[ EndFunctionKeyword <- 'end' ]], function()
	table.remove(scopeStack)
	-- pop the stored return value from the function stack and return it
	return {retn = table.remove(functionStack)}
end )
