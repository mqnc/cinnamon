
-- rule([[ FunctionDeclaration <- FunctionKeyword _ Identifier _ TemplateParameters _ Parameters _ ReturnDeclaration _ FunctionSpecifiers _ Terminal FunctionBody EndFunctionKeyword ]], functionGenerator )

function functionGenerator(sv, info)
-- used for functions and class members

	local access = sv[1] -- stores public/private/protected for methods
	local name = sv[3]
	local tparams = sv[5]
	local params = sv[7]
	local retn = sv[9]
	local specs = sv[11]
	local body = sv[14]
	local lastretn = sv[15]
	local colonInit = ""

	if info.rule == "SubscriptDeclaration" then
		name = {txt=mark .. "_invoke_subscript"}

		if retn.rule == "SubscriptAssignment" then
			-- the assignment becomes the first parameter
			name = {txt=mark .. "_assign_to_subscript"}
			params = utils.deepcopy(params)
			for i=#params, 1, -1 do
				params[i+1]=params[i]
			end
			params[1] = retn[3]
			retn = {choice="explicit", txt="void", subchoice="single"}
		end

	elseif info.rule == "CtorDeclaration" then
		name = {txt = classStack[#classStack].name}

		local init = body[2]
		if init.choice == "init" or init.choice == "delegate" then
			colonInit = ":" .. init.txt .. "\n"
		end
		body = {txt=body[3].txt}

	elseif info.rule == "DtorDeclaration" then
		name = {txt = "~" .. classStack[#classStack].name}

	elseif info.rule == "CallDeclaration" then
		name = {txt="operator()"}
	end

	local mangledName = name.txt
	if mangledName == "operator()" then
		mangledName = mark .. "_call"
	end

	--dump({specs=specs, name=name, params=params, retn=retn, body=body, lastretn=lastretn})

	local declaration = ""
	local definition = ""
	local temp = ""

	----------------------
	-- check specifiers --
	----------------------
	local export = false
 	local const = false
	local virtual = false
	local override = false
	local inline = false
	local static = false
	local kwargs = false
	local method = false

	if specs.choice == "list" then
		for i=1, #specs do
			if     specs[i].txt == "export" then export = true
			elseif specs[i].txt == "const" then const = true
			elseif specs[i].txt == "virtual" then virtual = true
			elseif specs[i].txt == "override" then override = true
			elseif specs[i].txt == "inline" then inline = true
			elseif specs[i].txt == "static" then static = true
			elseif specs[i].txt == "kwargs" then
				if info.rule == "SubscriptDeclaration" or info.rule == "SubscriptAssignment" or info.rule == "CallDeclaration" then
					transpilerError("keyword arguments not supported for call operator and subscript operator")
				end
 				kwargs = true
			end
		end
	end

	if info.rule=="MethodDeclaration" or
			info.rule=="CtorDeclaration" or
			info.rule=="DtorDeclaration" or
			info.rule=="SubscriptDeclaration" or
			info.rule=="SubscriptAssignment" or
			info.rule=="CallDeclaration" then
		method = true
		declaration = declaration .. access.choice .. ": "
	end

	if not export and not method then
		static = true
	end

	------------------------
	-- scan for templates --
	------------------------

	if method then
		definition = definition .. concatTemplateParams(classStack[#classStack].tparams, false)
	end

	local argTypeTempls = {}

	for i=1, #params do
		if params[i].type.empty then
			local att = {}
			att.kind = { ["choice"]="type", ["txt"]="typename" }
			att.name = { ["txt"]=mark .. "_T" .. params[i].name.txt }
			att.default = { ["empty"]=true, ["txt"]="" }

			if params[i].default.empty then
				att.type = { ["empty"]=true, ["txt"]="" }
			else
				att.type = { ["txt"]="rem_cv_ref<decltype(" .. params[i].default.txt .. ")>" }
			end

			table.insert(argTypeTempls, att)
		end
	end

	local allTemplates = utils.chain(tparams, argTypeTempls)

	declaration = declaration .. concatTemplateParams(allTemplates, true)
	definition = definition .. concatTemplateParams(allTemplates, false)

	----------------
	-- specifiers --
	----------------

	if inline then
		declaration = declaration .. "inline "
	end
	if static then
		declaration = declaration .. "static "
	end
	if virtual then
		declaration = declaration .. "virtual "
	end
	if override then
		declaration = declaration .. "override "
	end

	------------------------
	-- check return types --
	------------------------

	local arrowReturn = false
	local returnType = ""
	if info.rule ~= "CtorDeclaration" and info.rule ~= "DtorDeclaration" then
		if retn.choice == "explicit" then
			-- we will use the -> notation
			returnType = "auto "
			arrowReturn = true
		else
			if lastretn.retn.choice == nil or lastretn.retn.choice == "void" then
				returnType = "void "
			else
				returnType = "auto " -- return type deduction
			end
		end
		declaration = declaration .. returnType
		definition = definition .. returnType
	end

	-------------------
	-- function name --
	-------------------

	declaration = declaration .. name.txt
	if method then
		definition = definition .. classStack[#classStack].name ..
				concatTemplateArgs(classStack[#classStack].tparams) .. "::" .. name.txt
	else
		definition = definition .. name.txt
	end

	----------------
	-- parameters --
	----------------

	declaration = declaration .. "("
	definition = definition .. "("

	for i=1, #params do
		if i>1 then
			declaration = declaration .. ", "
			definition = definition .. ", "
		end

		if params[i].kind.choice == "constant" or params[i].kind.choice == "constref" then
			declaration = declaration .. "const "
			definition = definition .. "const "
		end

		if params[i].kind.choice ~= "variable" and
				params[i].kind.choice ~= "constant" and
				params[i].kind.choice ~= "constref" and
				params[i].kind.choice ~= "mutref" then
			transpilerError('function parameters must be "var", (implicitly) const, "ref" or "mutref"')
			declaration = declaration .. "ERROR"
			definition = definition .. "ERROR"
		end

		if params[i].type.empty then
			temp = mark .. "_T" .. params[i].name.txt .. " "
			declaration = declaration .. temp
			definition = definition .. temp
		else
			declaration = declaration .. params[i].type.txt .. " "
			definition = definition .. params[i].type.txt .. " "
		end

		if params[i].kind.choice == "constref" or params[i].kind.choice == "mutref" then
			declaration = declaration .. "& "
			definition = definition .. "& "
		end

		declaration = declaration .. params[i].name.txt
		definition = definition .. params[i].name.txt

		if params[i].default.empty == nil then
			if params[i].kind.choice == "constref" or params[i].kind.choice == "mutref" then
				transpilerError('reference parameters cannot have default values')
				declaration = declaration .. "ERROR"
				definition = definition .. "ERROR"
			end
			declaration = declaration .. " = " .. params[i].default.txt
		end
	end
	declaration = declaration .. ")"
	definition = definition .. ")"

	if const then
		declaration = declaration .. " const"
		definition = definition .. " const"
	end

	------------------
	-- arrow return --
	------------------

	local structForward = ""
	local struct = ""

	if arrowReturn then
		declaration = declaration .. " -> "
		definition = definition .. " -> "

		if retn.subchoice == "single" then
			declaration = declaration .. retn.txt
			definition = definition .. retn.txt

		elseif retn.subchoice == "tuple" then
			declaration = declaration .. "std::tuple<"
			definition = definition .. "std::tuple<"
			local retnfields = {}
			for i=1, #retn do
				table.insert(retnfields, retn[i].name)
			end
			temp = table.concat(retnfields, ", ") .. ">"
			declaration = declaration .. temp
			definition = definition .. temp

		elseif retn.subchoice == "struct" then

			-- template for propagating types
			local retnTempl = {}
			local typelist = {}
			for i=1, #retn do
				local rtpl = {}
				rtpl.kind = { ["choice"]="type", ["txt"]="typename" }
				rtpl.name = { ["txt"]=mark .. "_T" .. retn[i].name}
				rtpl.default = { ["empty"]=true, ["txt"]="" }
				table.insert(retnTempl, rtpl)
				table.insert(typelist, {["txt"]=retn[i].type})
			end

			-- struct NAME{
			local structName = mangledName .. "_" .. mark .. "_result"
			structForward = concatTemplateParams(retnTempl) .. "struct " .. structName .. ";\n"
			struct = concatTemplateParams(retnTempl) .. "struct " .. structName .. "{\n"

			-- list members
			local fields = {}
			for i=1, #retn do
				table.insert(fields, retnTempl[i].name.txt .. " " .. retn[i].name .. ";\n")
			end
			struct = struct .. table.concat(fields)

			-- construct tuple to construct from / cast to
			local tupleType = "std::tuple" .. concatTemplateArgs(retnTempl)

			-- constructor from tuple
			struct = struct .. structName .. "(" .. tupleType .. " " .. mark .. "_tup):\n"
			local coloninits = {}
			for i=1, #retn do
				table.insert(coloninits, retn[i].name .. "(std::get<" .. i-1 .. ">(" .. mark .. "_tup))")
			end
			struct = struct .. table.concat(coloninits, ",\n") .. "{}\n"

			-- cast to tuple operator
			struct = struct .. "operator " .. tupleType .. "(){\nreturn {"
			local bracelist = {}
			for i=1, #retn do
				table.insert(bracelist, retn[i].name)
			end
			struct = struct .. table.concat(bracelist, ", ") .. "};\n}\n};\n"

			declaration = declaration .. structName .. concatTemplateArgs(typelist)
			if method then
				definition = definition .. classStack[#classStack].name .. "::" .. structName .. concatTemplateArgs(typelist)
			else
				definition = definition .. structName .. concatTemplateArgs(typelist)
			end
		end
	end

	-----------------------
	-- keyword arguments --
	-----------------------

	local kwargmap = ""
	local kwargwrapdeclaration = ""
	local kwargwrapdefinition = ""
	if kwargs then
		kwargmap = kwargmap .. "\nstruct " .. mangledName .. "_" .. mark .. "_kwargmap{\n"
		for i=1, #params do
			kwargmap = kwargmap .. "static constexpr std::size_t " .. params[i].name.txt .. " = " .. (i-1) .. ";\n"
		end
		kwargmap = kwargmap .. "\n};\n"

		inl = ""
		if inline then inl = "inline " end

		local kwargTempl
 		if info.rule == "CtorDeclaration" then
			kwargTempl = utils.deepcopy(classStack[#classStack].tparams)
		else
			kwargTempl = utils.deepcopy(tparams)
		end
		local variadic = {}
		variadic.kind = { ["choice"]="type", ["txt"]="typename" }
		variadic.name = { ["txt"]="... Ts"}
		variadic.default = { ["empty"]=true, ["txt"]="" }
		variadic.type = { ["empty"]=true, ["txt"]="" }
		table.insert(kwargTempl, variadic)

		kwargwrapdeclaration = kwargwrapdeclaration .. concatTemplateParams(kwargTempl, true) .. inl .. "auto " .. mangledName .. "_" .. mark .. "_kwargwrap(Ts&&... ts)"

		if method and info.rule ~= "CtorDeclaration" then
			kwargwrapdefinition = "\n" .. concatTemplateParams(classStack[#classStack].tparams) ..
					concatTemplateParams(kwargTempl, false) .. "auto " .. classStack[#classStack].name ..
					concatTemplateArgs(classStack[#classStack].tparams) .. "::" .. mangledName .. "_" .. mark .. "_kwargwrap(Ts&&... ts)"
		else
			kwargwrapdefinition = concatTemplateParams(kwargTempl, false) .. "auto " .. mangledName .. "_" .. mark .. "_kwargwrap(Ts&&... ts)"
		end

		local call = {}
		for i=1, #params do
			if not params[i].default.empty then
				table.insert(call, "kw::arg_get<" .. (i-1) .. ">(std::forward<Ts>(ts)..., kw::arg<" ..
						(i-1) .. ">(" .. params[i].default.txt .. "))")
			else
				table.insert(call, "kw::arg_get<" .. (i-1) .. ">(std::forward<Ts>(ts)...)")
			end
		end
		if info.rule == "CtorDeclaration" then
			call = name.txt .. concatTemplateArgs(classStack[#classStack].tparams) .. "(\n" .. table.concat(call, ",\n") .. "\n)"
		else
			call = name.txt .. concatTemplateArgs(tparams) .. "(\n" .. table.concat(call, ",\n") .. "\n)"
		end

		if arrowReturn then
			kwargwrapdeclaration = kwargwrapdeclaration .. " -> " .. "rem_cv_ref<decltype(" .. call .. ")>"
			kwargwrapdefinition = kwargwrapdefinition .. " -> " .. "rem_cv_ref<decltype(" .. call .. ")>"
		end

		kwargwrapdeclaration = kwargwrapdeclaration .. ";\n"
		kwargwrapdefinition = kwargwrapdefinition .. "\n{\nreturn " .. call .. ";\n}\n"

	end

	------------------------
	-- declare and define --
	------------------------

	if name.txt == "main" then
		local fwdretn = ""
		local retn0 = "\nreturn 0;"
		if returnType ~= "void " then
			fwdretn = "return "
			retn0 = ""
		end
		local args = ""
		local forward = ""
		if #params > 0 then
			args = "int argc, char *argv[]"
			forward = "cinnamon::DynArray<cinnamon::String>(argv, argv + argc)"
		end

		codeSections.sourceFooter = codeSections.sourceFooter .. "cinnamon::Registry cinnamon::Registry::sRegistry{};\n"

		codeSections.sourceFooter = codeSections.sourceFooter .. "int main(" .. args .."){\n" .. fwdretn .. namespace .. "_" .. mark .. "_ns::main(" .. forward .. ");" .. retn0 .. "\n}\n"
	end

	declaration = declaration .. ";\n"
	definition = definition .. colonInit .. "\n{\n" .. body.txt .. "\n}\n"

	if method and info.rule ~= "CtorDeclaration" then
		if export and #allTemplates > 0 then
			codeSections.headerFunctionDefinitions = codeSections.headerFunctionDefinitions .. definition .. kwargwrapdefinition
		else
			codeSections.sourceFunctionDefinitions = codeSections.sourceFunctionDefinitions .. definition .. kwargwrapdefinition
		end
		return {txt=struct .. "\n" .. declaration .. kwargmap .. kwargwrapdeclaration} -- will go to the class declaration
	elseif info.rule == "CtorDeclaration" then
		if export and #allTemplates > 0 then
			codeSections.headerFunctionDefinitions = codeSections.headerFunctionDefinitions .. definition .. kwargwrapdefinition
			codeSections.headerFunctionDeclarations = codeSections.headerFunctionDeclarations .. kwargmap .. kwargwrapdeclaration
		else
			codeSections.sourceFunctionDefinitions = codeSections.sourceFunctionDefinitions .. definition .. kwargwrapdefinition
			codeSections.sourceFunctionDeclarations = codeSections.sourceFunctionDeclarations .. kwargmap .. kwargwrapdeclaration
		end
		return {txt=struct .. "\n" .. declaration} -- will go to the class declaration
	else -- free function
		if export then
			codeSections.headerClassForwardDeclarations = codeSections.headerClassForwardDeclarations .. structForward
			codeSections.headerClassDeclarations = codeSections.headerClassDeclarations .. struct
			codeSections.headerFunctionDeclarations = codeSections.headerFunctionDeclarations .. declaration .. kwargmap .. kwargwrapdeclaration
		else
			codeSections.sourceClassForwardDeclarations = codeSections.sourceClassForwardDeclarations .. structForward
			codeSections.sourceClassDeclarations = codeSections.sourceClassDeclarations .. struct
			codeSections.sourceFunctionDeclarations = codeSections.sourceFunctionDeclarations .. declaration .. kwargmap .. kwargwrapdeclaration
		end
		if export and #allTemplates > 0 then -- there are templated parameters
			codeSections.headerFunctionDefinitions = codeSections.headerFunctionDefinitions .. definition .. kwargwrapdefinition
		else
			codeSections.sourceFunctionDefinitions = codeSections.sourceFunctionDefinitions .. definition .. kwargwrapdefinition
		end
	end

	return {txt=""}
end
