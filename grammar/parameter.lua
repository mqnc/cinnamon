
function concatTemplateParams(params, withDefaults)
	if #params == 0 then
		if params.force then
			return "template <>\n"
		else
			return ""
		end
	end

	list = {}
	for i=1, #params do
		local buf
		if params[i].kind.choice == "value" then
			if params[i].type.empty then
				buf = "auto"
			else
				buf = params[i].type.txt
			end
		elseif params[i].kind.choice == "type" then
			buf = "typename"
		else
			transpilerError('Template parameters must be "type" or "val"')
			buf = "ERROR"
		end

		buf = buf .. " " .. params[i].name.txt

		if withDefaults then
			if params[i].kind.choice == "value" and params[i].default.empty == nil then
				buf = buf .. "=" .. params[i].default.txt
			elseif params[i].kind.choice == "type" and params[i].type.empty == nil then
				buf = buf .. "=" .. params[i].type.txt
			end
		end
		table.insert(list, buf)
	end
	return "template <" .. table.concat(list, ", ") .. " >\n"
end

function concatTemplateArgs(args)
	if #args == 0 then
		if args.force then
			return "<>"
		else
			return ""
		end
	end

	list = {}
	for i=1, #args do
		if args[i].name then
			table.insert(list, args[i].name.txt)
		else
			table.insert(list, args[i].txt)
		end
	end
	return "<" .. table.concat(list, ", ") .. " >"
end

rule([[ TemplateParameters <- (LTemplateBrace _ ParameterDeclarationList _ RTemplateBrace)? ]], function(sv, info)
	if #sv == 0 then
		return {force = false}
	end
	params = utils.deepcopy(sv[3])
	params.force = true
	return params
end )

rule([[ NoTemplateParameters <- "" ]], function(sv, info)
	return {force = false}
end )

rule([[ Parameters <- ParameterList / NoParameters ]], basic.forward(1) )
rule([[ NoParameters <- (LParen _ RParen)? ]], function(sv, info) return {} end)
rule([[ ParameterList <- LParen _ ParameterDeclarationList _ RParen ]], basic.forward(3) )

rule([[ BracketParameters <- BracketParameterList / NoBracketParameters ]], basic.forward(1) )
rule([[ NoBracketParameters <- LBracket _ RBracket ]], function(sv, info) return {} end)
rule([[ BracketParameterList <- LBracket _ ParameterDeclarationList _ RBracket ]], basic.forward(3) )

rule([[ ParameterDeclaration <- ParameterKind _ Identifier _ (~TypeDeclareOperator ~_ ObjectOrSpecializedTypeOrFunction / Empty) _ (~AssignOperator ~_ Expression / Empty) ]], function(sv, info)
	if #scopeStack > 0 then -- we don't care about global scope
		table.insert(scopeStack[#scopeStack], sv[3].txt)
	end
	return {kind = sv[1], name = sv[3], type = sv[5], default = sv[7]}
end )

rule([[ ParameterDeclarationList <- (ParameterDeclaration (_ Comma _ ParameterDeclaration)*)? ]], basic.listFilter)

rule([[ ParameterKind <- TypeParameter / ValueParameter / ReferenceParameter / MutableReferenceParameter / VariableParameter / ConstantParameter ]], basic.choice("type", "value", "constref", "mutref", "variable", "constant") )
rule([[ TypeParameter <- 'type' ]], 'typename' )
rule([[ ValueParameter <- 'val' ]], 'auto' )
rule([[ VariableParameter <- 'var' ]], 'T' )
rule([[ ConstantParameter <- '' ]], 'const auto' )
rule([[ ReferenceParameter <- 'ref' ]], 'const auto&' )
rule([[ MutableReferenceParameter <- 'mutref' ]], 'auto&' )
