


-- rule([[ ClassDeclaration <- ClassKeyword _ Identifier _ ClassTemplateParameters _ OptionalInheritance _ ClassSpecifiers _ SilentTerminal ClassBody EndClassKeyword ]], classGenerator )

function classGenerator(sv, info)

	local name = sv[3]
	local tparams = sv[5]
	local inherit = sv[7]
	local specs = sv[9]
	local body = sv[12]
	local classStackEntry = sv[13].retn

	local declaration = ""
	local definition = ""
	local temp = ""

	local export = false
	if specs.choice == "list" then
		for i=1, #specs do
			if     specs[i].txt == "export" then export = true end
		end
	end

	declaration = declaration .. concatTemplateParams(tparams, false)
	definition = definition .. concatTemplateParams(tparams, true)

	declaration = declaration .. "class " .. name.txt
	definition = definition .. "class " .. name.txt

	if inherit.choice == "inherit" then
		definition = definition .. ":" .. inherit.txt
	end

	--[[
	local fieldRefList = {}
	for i=1, #fieldList do
		table.insert(fieldRefList, "contains_refs<rem_cv_ref<decltype(" .. fieldList[i] .. ")> >()")
	end
	local containsrefsreturn = "false"
	if #fieldList > 0 then
		containsrefsreturn = table.concat(fieldRefList, " \n|| ")
	end

	local containsrefs = "\nstatic constexpr bool contains_refs_m(){\nreturn " .. containsrefsreturn .. ";\n}\n"
	]]
	local containsrefs = ""

	declaration = declaration .. ";\n"
	definition = definition .. "\n{\n" .. body.txt .. containsrefs .. "\n};\n"

	if export then
		codeSections.headerClassForwardDeclarations = codeSections.headerClassForwardDeclarations .. declaration
		codeSections.headerClassDeclarations = codeSections.headerClassDeclarations .. definition
	else
		codeSections.sourceClassForwardDeclarations = codeSections.sourceClassForwardDeclarations .. declaration
		codeSections.sourceClassDeclarations = codeSections.sourceClassDeclarations .. definition
	end

	fieldList = nil

	return {txt=""}
end
