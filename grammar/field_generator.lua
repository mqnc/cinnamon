
-- rule([[ FieldDeclaration <- FieldAccess _ Identifier _ AssignOperator _ Assigned _ FieldSpecifiers _ Terminal ]], fieldGenerator )
function fieldGenerator(sv, info)
		-- pun intended

	local access = sv[1]
	local static = false
	local id = sv[3]
	local assigned = sv[7]
	local specs = sv[9]
	local const = string.find(access.txt, "const") ~= nil

	local result = {}

	if specs.choice == "list" then
		for i=1, #specs do
			if     specs[i].txt == "static" then static = true end
		end
	end

	table.insert(fieldList, id.txt)
	table.insert(result, access.txt .. " ")

	if static and not const then
		table.insert(result, "static rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. id.txt .. ";\n")
		codeSections.sourceFunctionDefinitions = codeSections.sourceFunctionDefinitions .. "rem_cv_ref<decltype(" .. assigned.txt .. ")> " ..
				classStack[#classStack].name .. "::" .. id.txt .. " = " .. assigned.txt .. ";\n"
	else
		if static then
			table.insert(result, "static rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. id.txt .. " = " .. assigned.txt .. ";\n")
		else
			table.insert(result, "rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. id.txt .. " = " .. assigned.txt .. ";\n")
		end
	end

	return {txt=table.concat(result)}
end
