
rule([[ LocalVariableDeclaration <- VariableDeclaration ]], variable_generator)
rule([[ GlobalVariableDeclaration <- VariableDeclaration ]], variable_generator)

rule([[ VariableDeclaration <- VariableDeclaree _ TemplateParameters _ AssignOperator _ Assigned _ VariableSpecifiers _ Terminal  ]], basic.tree )
rule([[ VariableDeclaree <- ConstSpecifier _ (IdentifierListMulti / Identifier) ]], function(sv, info)
	local const = sv[1].txt

	local ids = sv[3]
	local idtxt
	if #ids > 1 then
		idtxt = "[" .. ids.txt .. "]"
	else
		idtxt = ids.txt
	end

	if #scopeStack > 0 then
		if #ids == 0 then
			table.insert(scopeStack[#scopeStack], ids.txt)
		else
			for i=1, #ids do
				table.insert(scopeStack[#scopeStack], ids[i].txt)
			end
		end
	end

	return {const=const, idtxt=idtxt, txt=const .. " auto " .. idtxt}
end )

rule([[ Assignment <- TieToValues / AssignToSubscript / AssignToValue ]], basic.concat )

rule([[ AssignToSubscript <- Expression _ LBracket _ (InsertComma ExpressionList / Empty Empty) _ RBracket _ AssignOperator _ Assigned _ Terminal ]],
		mark .. "_assign_to_subscript({12}, {1}{5} {6}){14}")

--rule([[ AssignToCall <- Identifier _ LParen _ (InsertComma ExpressionList / Empty Empty) _ RParen _ AssignOperator _ Assigned _ Terminal ]],
--		mark .. "_assign_to_call({12}, {1}{5} {6}){14}")

rule([[ AssignToValue <- Expression _ AssignOperator _ Assigned _ Terminal ]], basic.concat )

rule([[ TieToValues <- ExpressionListMulti _ AssignOperator _ Assigned _ Terminal ]], function(sv, info)
	return {txt ="std::tie(" .. sv[1].txt .. ") " .. sv[2].txt .. " = " ..
			sv[4].txt .. "static_cast<std::tuple<rem_cv_ref<decltype(" ..
			table.concat(utils.fields(sv[1].list, "txt"), ")>, rem_cv_ref<decltype(") ..
 			")> > >(" .. sv[5].txt .. ")" .. sv[6].txt .. sv[7].txt}
end )

rule([[ Assigned <- AssignedTuple / Expression ]], basic.concat )
rule([[ AssignedTuple <- ExpressionListMulti ]], 'std::make_tuple({1})' )

rule([[ ConstSpecifier <- ConstantType / VariableType ]], basic.forward(1) )
rule([[ InsertAuto <- '' ]], ' auto' )
rule([[ ConstantType <- 'const' ]], 'const' )
rule([[ VariableType <- 'var' ]], '' )
table.insert(keywords, "ConstSpecifier")

rule([[ AssignOperator <- ':=' ]], "=" )
rule([[ TypeDeclareOperator <- ':' ]], "=" )

rule([[ VariableSpecifiers <- VariableSpecifierList / Empty ]], basic.choice("list", "none") )
rule([[ VariableSpecifierList <- SpecifierIndicator _ SomeVariableSpecifiers ]], basic.forward(3) )
rule([[ SomeVariableSpecifiers <- VariableSpecifier (_ Comma _ VariableSpecifier)* ]], basic.listFilter)
rule([[ VariableSpecifier <- VariableExport / VariableStatic ]], basic.forward(1) )
rule([[ VariableExport <- "export" ]], "export")
rule([[ VariableStatic <- "persistent" ]], "persistent")

table.insert(globalStatements, "GlobalVariableDeclaration")
table.insert(localStatements, "LocalVariableDeclaration")
table.insert(localStatements, "Assignment")
