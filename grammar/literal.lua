
rule([[ BooleanLiteral <- "true" / "false" ]], basic.match )
rule([[ CharLiteral <- '\'' (('\\' .) / .) '\'' ]], basic.match )
rule([[ StringLiteral <- '"' (('\\' .) / (!'"' .))* '"' ]], "std::string({match})" )
rule([[ FloatLiteral <- [0-9]+ '.' [0-9]+ (('e' / 'E') '-'? [0-9]+)? ]], basic.match )
rule([[ IntegerLiteral <- [0-9]+ ]], basic.match )
rule([[ HexLiteral <- '0' ('x' / 'X') [0-9]+ ]], basic.match )

rule([[ Literal <- (BooleanLiteral / CharLiteral / StringLiteral / FloatLiteral / IntegerLiteral / HexLiteral) WordEnd ]], basic.forward(1) )

table.insert(atomics, "Literal")
table.insert(keywords, "BooleanLiteral")
