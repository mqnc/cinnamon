
function importer(module, alias, tokens, export)
	-- "import name" will set tokens to nil
	-- "from name import things" will have tokens

	local alreadyDone = false
	for i=1, #fileQueue do
		if fileQueue[i] == module.txt then
			alreadyDone = true
			break
		end
	end

	local file = module.txt
	if module.first ~= nil then
		file = module.first
	end

	if not alreadyDone then
		table.insert(fileQueue, file)
	end

	local include = '#include "' .. file .. '.hpp"\n'
	local usings = ""

	if tokens == nil then
		usings = usings .. "namespace " .. alias .. " = " .. module.txt .. "_" .. mark .. "_ns;\n"
		modules[alias] = true
	elseif tokens.txt == "*" then
		usings = usings .. "using namespace " .. module.txt .. "_" .. mark .. "_ns;\n"
	elseif #tokens > 0 then
		for i = 1,#tokens do
			using = "using "
			if tokens[i].alias ~= nil then
				using = using .. tokens[i].alias .. " = "
			end
			if module.first ~= nil then
				using = using .. module.first .. "_" .. mark .. "_ns" .. module.rest .. "::"
			else
				using = using .. module.txt .. "_" .. mark .. "_ns::"
			end
			using = using .. tokens[i].txt .. ";\n"
			usings = usings .. using
		end
	end

	if export then
		codeSections.headerIncludes = codeSections.headerIncludes .. include
		codeSections.headerUsingDeclarations = codeSections.headerUsingDeclarations .. usings
	else
		codeSections.sourceIncludes = codeSections.sourceIncludes .. include
		codeSections.sourceUsingDeclarations = codeSections.sourceUsingDeclarations .. usings
	end
end


rule([[ ImportStatement <- ImportKeyword _ Identifier _ (ImportAlias / Empty) _ (ImportExport / Empty) _ Terminal ]],
function(sv, info)
	module = sv[3]
	alias = module.txt
	if sv[5].empty == nil then
		alias = sv[5].txt
	end
	export = sv[7].empty == nil
	importer(module, alias, nil, export)
end )

rule([[ FromImportStatement <- FromKeyword _ ScopeResChain _ ImportKeyword _ (ImportTokenList / Asterisk) _ (ImportExport / Empty) _ Terminal ]],
function(sv, info)
	module = sv[3]
	tokens = sv[7]
	export = sv[9].empty == nil
	importer(module, module.txt, tokens, export)
end )

table.insert(globalStatements, "ImportStatement")
table.insert(globalStatements, "FromImportStatement")

rule([[ ImportTokenList <- ImportToken (_ Comma _ ImportToken)* ]], basic.listFilter)
rule([[ ImportToken <- Identifier _ (ImportAlias / Empty) ]], function(sv, info)
	result = {}
	if sv[3].empty == nil then
		result.alias = sv[3].txt
	end
	result.txt = sv[1].txt
	return result
end )
rule([[ ImportAlias <- As _ Identifier ]], basic.forward(3))
rule([[ As <- "as" ]])
rule([[ ScopeResChain <- Identifier (_ DblColon _ Identifier)* ]], function(sv, info)
	conc = basic.concat(sv, info)
	conc.first = sv[1].txt
	conc.rest = ""
	for i=3, #sv, 2 do
		conc.rest = conc.rest .. sv[i].txt
	end
	return conc
end )
rule([[ ImportExport <- SpecifierIndicator _ FunctionExport ]])
rule([[ ImportKeyword <- "import" ]])
rule([[ FromKeyword <- "from" ]])
