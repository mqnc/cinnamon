
-- a chain is something like this: module::myobj<int, 4>::subclass.method(x:=3)[0, 0].get()

-- typeof(...) / DynArray{Int8} (beginning of a chain)
rule([[ ScopedObjectOrSpecializedTypeOrFunction <- ObjectOrSpecializedTypeOrFunction / Self / TypeofExpression ]], function(sv, info)

	--dump(sv)
	--dump(scopeStack)
	--print("-------------")

	if info.choice == 1 then
		id = sv[1].id

		local scope = ""

		-- don't prepend scope when we're in global scope
		if #scopeStack > 0 then
			scope = namespace .. "_" .. mark .. "_ns"

			if modules[id] then
				-- identifier names a module, dont prepend another module name
				scope = ""
			end

			for i=1, #scopeStack do
				for j=1, #(scopeStack[i]) do
					if scopeStack[i][j] == id then -- identifier found in local scope, dont prepend module namespace
						scope = ""
						break
					end
				end
				if scope == "" then
					break
				end
			end
		end

		result = utils.deepcopy(sv[1])
		if scope ~= "" then
			result.txt = scope .. "::" .. result.txt
		end
		return result
	else
		return basic.forward(1)(sv, info)
	end
end )

rule([[ Self <- "self" ]], "(*this)")
table.insert(keywords, "Self")

rule([[ AssertMacro <- "assert" ]], "assert")
table.insert(keywords, "AssertMacro")
table.insert(atomics, "AssertMacro")

-- only this can be in the beginning of a chain
rule([[ ScopedResolvedObject <- ScopedObjectOrSpecializedTypeOrFunction (_ ScopeResolutionOperator _ ObjectOrSpecializedTypeOrFunction)* ]], basic.concat_and_tree)

-- std::enable_if{true, int}::value
rule([[ ResolvedObject <- ObjectOrSpecializedTypeOrFunction (_ ScopeResolutionOperator _ ObjectOrSpecializedTypeOrFunction)* ]], basic.concat_and_tree)

table.insert(atomics, "ScopedResolvedObject")

-- .member / [arg] / (arg) / (kwarg:=7)
rule([[ CallChainAppendix <- ~Point ~_ ResolvedObject / Subscript / Call / KwargsCall ]], basic.choice("member", "subscript", "call", "kwargs"))

rule([[ ObjectOrSpecializedTypeOrFunction <- Identifier _ (TemplateArguments / Empty) ]], function(sv, info)
	return{
		txt = sv[1].txt .. sv[2].txt .. sv[3].txt,
		id = sv[1].txt,
		targs = sv[3].txt
	}
end )

rule([[ TypeofExpression <- Typeof _ LParen _ Expression _ RParen ]], function(sv, info)
	local result = {}
	result.txt = "rem_cv_ref<decltype(" .. sv[5].txt .. ")>"
	result.id = result.txt
	result.targs = ""
	return result
end )
rule([[ Typeof <- "typeof" ]])
table.insert(keywords, "Typeof")

rule([[ ScopeResolutionOperator <- "::" ]], "::")
rule([[ TemplateArguments <- LTemplateBrace _ OptionalExpressionList _ RTemplateBrace ]], basic.concat)
rule([[ LTemplateBrace <- LBrace ]], "<" )
rule([[ RTemplateBrace <- RBrace ]], ">" )

rule([[ Call <- LParen _ OptionalExpressionList _ RParen ]], basic.forward(3) )
rule([[ Subscript <- (LBracket _ Empty _ RBracket / LBracket _ ExpressionList _ RBracket) !(_ AssignOperator) ]], basic.forward(3) ) -- makes it easier with commas
rule([[ KwargsCall <- LParen _ KwargsList _ RParen ]], basic.forward(3) )

rule([[ KwargsList <- (Expression _ Comma _)* KwargAssignment (_ Comma _ KwargAssignment)* ]], basic.listFilter)
rule([[ KwargAssignment <- Identifier ~_ ~AssignOperator ~_ Expression ]], basic.tree)

-- the string.sub part is really ugly, maybe I should rather have (kwargs) an optional appendix to ExplicitObjectOrSpecializedTypeOrFunction
rule([[ Chain <- Atomic (~_ CallChainAppendix)* ]], function(sv, info)

	result = sv[1].txt

	for i=2, #sv do
		if sv[i].choice == "member" then
			result = result .. "." .. sv[i].txt
		elseif sv[i].choice == "call" then
			result = result .. "(" .. sv[i].txt .. ")"
		elseif sv[i].choice == "subscript" then
			if sv[i].empty then
				result = mark .. "_invoke_subscript(" .. result .. ")"
			else
				result = mark .. "_invoke_subscript(" .. result .. ", " .. sv[i].txt .. ")"
			end
		elseif sv[i].choice == "kwargs" then
			if i==2 then -- we are calling a function or a class constructor
				local callee = sv[1]
				local params = sv[i]
				local calleeWithoutTargs = callee.txt
				local calleeTargs = ""
				if callee.rule == "ResolvedObject" or callee.rule == "ScopedResolvedObject" then
					calleeTargs = callee[#callee].targs
					calleeWithoutTargs = string.sub(callee.txt, 1, #(callee.txt)-#(calleeTargs))
				end
				local temp = {}
				for i=1,#params do
					if params[i].rule == "KwargAssignment" then
						table.insert(temp, "kw::arg<" .. calleeWithoutTargs .. "_" .. mark .. "_kwargmap" .. "::" .. params[i][1].txt .. ">(" .. params[i][2].txt .. ")")
					else
						table.insert(temp, params[i].txt)
					end
				end

				result = calleeWithoutTargs .. "_" .. mark .. "_kwargwrap" .. calleeTargs .. "(\n" .. table.concat(temp, ",\n") .. "\n)"

			elseif sv[i-1].choice == "member" then

				local method = sv[i-1]
				local object = string.sub(result, 1, #result-#(method.txt)-1)
				local methodTargs = method[#method].targs
				local methodWithoutTargs = string.sub(method.txt, 1, #(method.txt)-#(methodTargs))
				local params = sv[i]
				local class = "rem_cv_ref<decltype(" .. object .. ")>"

				local temp = {}
				for i=1,#params do
					if params[i].rule == "KwargAssignment" then
						table.insert(temp, "kw::arg<" .. class .. "::" .. methodWithoutTargs .. "_" .. mark .. "_kwargmap::" .. params[i][1].txt .. ">(" .. params[i][2].txt .. ")")
					else
						table.insert(temp, params[i].txt)
					end
				end

				result = "(" .. object .. ")." .. methodWithoutTargs .. "_" .. mark .. "_kwargwrap" .. methodTargs .. "(\n" .. table.concat(temp, ",\n") .. "\n)"
			else
				transpilerError("Keyword arguments are only supported if the callee name (possibly with template arguments) appears directly before the parentheses.")
				result = result .. "(KWARGS NOT ALLOWED)"
			end

		end
	end

	return {txt=result}
end )
