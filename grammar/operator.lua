

OperatorClasses = {
	{
		name = "Prefix",
		order = "rtl",
		operators = {
			{peg="'+'", cpp="+({>})"},
			{peg="'-'", cpp="-({>})"},
			{peg="'not' WordEnd", cpp="!({>})"},
			{peg="'bitnot' WordEnd", cpp="~({>})"},
			{peg="'sizeof' WordEnd", cpp="sizeof({>})"},
			{peg="'#'", cpp="({>}).size()"}
		}
	}, {
		name = "Exponentiation",
		order = "rtl",
		operators = {
			{peg="'^'", cpp="pow(({<}), ({>}))"}
		}
	}, {
		name = "Multiplication",
		order = "ltr",
		operators = {
			{peg="'*'", cpp="({<})*({>})"},
			{peg="'/'", cpp="double({<})/double({>})"},
			{peg="'div' WordEnd", cpp="int({<})/int({>})"},
			{peg="'mod' WordEnd", cpp="({<})%%({>})"} -- % is the gsub escape character
		}
	}, {
		name = "Addition",
		order = "ltr",
		operators = {
			{peg="'+'", cpp="({<})+({>})"},
			{peg="'-'", cpp="({<})-({>})"}
		}
	}, {
		name = "Shifting",
		order = "ltr",
		operators = {
			{peg="'<<'", cpp="({<})<<({>})"},
			{peg="'>>'", cpp="({<})>>({>})"}
		}
	}, {
		name = "BitConjunction",
		order = "ltr",
		operators = {
			{peg="'bitand' WordEnd", cpp="({<})&({>})"}
		}
	}, {
		name = "BitExclusiveDisjunction",
		order = "ltr",
		operators = {
			{peg="'bitxor' WordEnd", cpp="({<})^({>})"}
		}
	}, {
		name = "BitDisjunction",
		order = "ltr",
		operators = {
			{peg="'bitor' WordEnd", cpp="({<})|({>})"}
		}
	}, {
		name = "Comparison",
		order = "ltr",
		operators = {
			{peg="'=='", cpp="({<})==({>})"},
			{peg="'!='", cpp="({<})!=({>})"},
			{peg="'<='", cpp="({<})<=({>})"},
			{peg="'>='", cpp="({<})>=({>})"},
			{peg="'<'", cpp="({<})<({>})"},
			{peg="'>'", cpp="({<})>({>})"}
		}
	}, {
		name = "Conjunction",
		order = "ltr",
		operators = {
			{peg="'and' WordEnd", cpp="({<})&&({>})"}
		}
	}, {
		name = "ExclusiveDisjunction",
		order = "ltr",
		operators = {
			{peg="'xor' WordEnd", cpp="!({<})!=!({>})"}
		}
	}, {
		name = "Disjunction",
		order = "ltr",
		operators = {
			{peg="'or' WordEnd", cpp="({<})||({>})"}
		}
	}, {
		name = "Conditional",
		order = "rtl",
		operators = {
			{peg="'?' ~_ Expression ~_ ':'", cpp="({<})? ({1}):({>})"}
		}
	}
}

-- in addition to peg and cpp there can be a field "special" containing a function that will get the snippet list (like this {"cond", "?", "opttrue", ":", "optfalse"} where the arguments are defined by the cpp string. it has to return a string containing the result code. this was added for keyword arguments which are solved differently by now
