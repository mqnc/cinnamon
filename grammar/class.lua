
rule([[ ClassDeclaration <- ClassKeyword _ ClassName _ ClassTemplateParameters _ OptionalInheritance _ ClassSpecifiers _ SilentTerminal ClassBody EndClassKeyword ]], classGenerator )

table.insert(globalStatements, "ClassDeclaration")

rule([[ ClassKeyword <- "class" ]], function(sv, info)
	table.insert(classStack, {})
	fieldList = {}
	return {}
end )

rule([[ ClassName <- Identifier ]], function(sv, info)
	classStack[#classStack].name = sv[1].txt
	return {txt=sv[1].txt}
end )

rule([[ ClassTemplateParameters <- TemplateParameters ]], function(sv, info)
	tparams = sv[1]
	classStack[#classStack].tparams = tparams
	return tparams
end )

rule([[ OptionalInheritance <- Inheritance / NoInheritance ]], basic.choice("inherit", "none"))
rule([[ NoInheritance <- "" ]])
rule([[ Inheritance <- InheritKeyword _ InheritanceList ]], basic.forward(3))
rule([[ InheritKeyword <- "inherits" ]])
rule([[ InheritanceList <- InheritanceListItem (_ Comma _ InheritanceListItem)* ]], basic.listFilter)
rule([[ InheritanceListItem <- PublicInheritanceItem / ProtectedInheritanceItem / PrivateInheritanceItem ]], basic.concat)

rule([[ PublicInheritanceItem <- ObjectOrSpecializedTypeOrFunction ]], "public {1}")
rule([[ ProtectedInheritanceItem <- LBracket _ ObjectOrSpecializedTypeOrFunction _ RBracket ]], "protected {3}")
rule([[ PrivateInheritanceItem <- LParen _ ObjectOrSpecializedTypeOrFunction _ RParen ]], "private {3}")

rule([[ ClassSpecifiers <- ClassSpecifierList / Empty ]], basic.choice("list", "none") )
rule([[ ClassSpecifierList <- SpecifierIndicator _ SomeClassSpecifiers ]], basic.forward(3) )
rule([[ SomeClassSpecifiers <- ClassSpecifier (_ Comma _ ClassSpecifier)* ]], basic.listFilter)
rule([[ ClassSpecifier <- ClassExport ]], basic.match )
rule([[ ClassExport <- "export" ]])

rule([[ ClassBody <- Skip (!EndKeyword ClassMemberDeclaration Skip)* ]], basic.concat )

rule([[ ClassMemberDeclaration <- CppGlobal / CppExpression / FieldDeclaration / MethodDeclaration / OperatorDeclaration / ScopeInspector / SyntaxError ]], basic.concat )

rule([[ OperatorDeclaration <- CtorDeclaration / DtorDeclaration / CallDeclaration / SubscriptDeclaration ]], basic.concat )

rule([[ MemberSpecifiers <- SomeMemberSpecifiers / NoMemberSpecifiers ]], basic.choice("list", "none") )
rule([[ SomeMemberSpecifiers <- SpecifierIndicator _ MemberSpecifierList ]], basic.forward(3) )
rule([[ NoMemberSpecifiers <- "" ]])
rule([[ MemberSpecifierList <- MemberSpecifier (_ Comma _ MemberSpecifier)* ]], basic.listFilter)
rule([[ MemberSpecifier <- MemberConst / MemberVirtual / MemberOverride / MemberInline / MemberStatic / MemberKwargs ]], basic.forward(1))

rule([[ MemberConst <- "const" ]], "const")
rule([[ MemberVirtual <- "virtual" ]], "virtual")
rule([[ MemberOverride <- "override" ]], "override")
rule([[ MemberInline <- "inline" ]], "inline")
rule([[ MemberStatic <- "shared" ]], "static")
rule([[ MemberKwargs <- "kwargs" ]], "kwargs")

-- METHOD / CALL / SUBSCRIPT

rule([[ MethodDeclaration <- MethodAccess _ Identifier _ TemplateParameters _ Parameters _ ReturnDeclaration _ MemberSpecifiers _ Terminal FunctionBody EndFunctionKeyword ]], functionGenerator )

rule([[ MethodAccess <- PublicMethod / ProtectedMethod / PrivateMethod ]], function(sv, info)
	table.insert(scopeStack, {})
	table.insert(functionStack, {})
	return basic.choice("public", "protected", "private")(sv, info)
end )

rule([[ PublicMethod <- MethodKeyword ]])
rule([[ ProtectedMethod <- LBracket _ MethodKeyword _ RBracket ]])
rule([[ PrivateMethod <- LParen _ MethodKeyword _ RParen ]])
rule([[ MethodKeyword <- "method" ]])

rule([[ CallDeclaration <- MethodAccess _ Empty _ NoTemplateParameters _ Parameters _ ReturnDeclaration _ MemberSpecifiers _ Terminal FunctionBody EndFunctionKeyword ]], functionGenerator )

rule([[ SubscriptDeclaration <- MethodAccess _ Empty _ NoTemplateParameters _ BracketParameters _ (SubscriptAssignment / ReturnDeclaration) _ MemberSpecifiers _ Terminal FunctionBody EndFunctionKeyword ]], functionGenerator )

rule([[ SubscriptAssignment <- AssignOperator _ ParameterDeclaration ]], basic.tree )

-- CONSTRUCTOR

rule([[ CtorDeclaration <- CtorAccess _ Empty _ NoTemplateParameters _ Parameters _ Empty _ MemberSpecifiers _ Terminal CtorBody EndScope ]], functionGenerator )

rule([[ CtorBody <- Skip Inits CtorDtorBody ]], basic.tree )
rule([[ Inits <- InitList / Delegation / NoInits ]], basic.choice("init", "delegate", "none") )
rule([[ NoInits <- "" ]])

rule([[ CtorAccess <- PublicCtor / ProtectedCtor / PrivateCtor ]], function(sv, info)
	table.insert(scopeStack, {})
	return basic.choice("public", "protected", "private")(sv, info)
end )
rule([[ PublicCtor <- CtorKeyword ]])
rule([[ ProtectedCtor <- LBracket _ CtorKeyword _ RBracket ]])
rule([[ PrivateCtor <- LParen _ CtorKeyword _ RParen ]])
rule([[ CtorKeyword <- "ctor" ]], function()
	return {txt=classStack[#classStack].name}
end )

rule([[ InitList <- InitKeyword _ Terminal _ FieldAssignmentList _ EndKeyword ]], basic.forward(5))
rule([[ InitKeyword <- "init" ]])
rule([[ FieldAssignmentList <- FieldAssignment _ Terminal (_ FieldAssignment _ Terminal)* ]], basic.listFilter)
rule([[ FieldAssignment <- Self _ Point _ Identifier _ AssignOperator _ Assigned ]], "{5}{{9}}" )

rule([[ Delegation <- CtorKeyword _ LParen _ ExpressionList _ RParen _ SilentTerminal ]], basic.concat )

rule([[ CtorDtorBody <- Skip (!EndKeyword LocalStatement Skip)* ]], basic.concat )

-- DESTRUCTOR

rule([[ DtorDeclaration <- DtorAccess _ Empty _ NoTemplateParameters _ NoParameters _ Empty _ MemberSpecifiers _ Terminal CtorDtorBody EndScope ]], functionGenerator )

rule([[ DtorAccess <- PublicDtor / ProtectedDtor / PrivateDtor ]], basic.choice("public", "protected", "private"))
rule([[ PublicDtor <- DtorKeyword ]])
rule([[ ProtectedDtor <- LBracket _ DtorKeyword _ RBracket ]])
rule([[ PrivateDtor <- LParen _ DtorKeyword _ RParen ]])
rule([[ DtorKeyword <- "dtor" ]], function(sv, info)
	table.insert(scopeStack, {})
end )

-- FIELD

rule([[ FieldDeclaration <- FieldAccess _ Identifier _ AssignOperator _ Assigned _ FieldSpecifiers _ Terminal ]], fieldGenerator )

rule([[ FieldAccess <- PublicField / ProtectedField / PrivateField ]], basic.forward(1))

rule([[ PublicField <- ConstSpecifier ]], "public: {1}")
rule([[ ProtectedField <- LBracket _ ConstSpecifier _ RBracket ]], "protected: {3}")
rule([[ PrivateField <- LParen _ ConstSpecifier _ RParen ]], "private: {3}")

rule([[ FieldSpecifiers <- SomeFieldSpecifiers / Empty ]], basic.choice("list", "none") )
rule([[ SomeFieldSpecifiers <- SpecifierIndicator _ FieldSpecifierList ]], basic.forward(3))
rule([[ FieldSpecifierList <- FieldSpecifier (_ Comma _ FieldSpecifier)* ]], basic.listFilter)
rule([[ FieldSpecifier <- MemberStatic ]], basic.forward(1) )

rule([[ EndClassKeyword <- 'end' ]], function()
	-- pop the stored return value from the class stack and return it
	return {retn = table.remove(classStack)}
end )
