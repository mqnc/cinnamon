
variable_generator = function(sv, info)
	sv = sv[1]

	local const = sv[1].const
	local idtxt = sv[1].idtxt
	local tparams = sv[3]
	local assigned = sv[7]
	local specs = sv[9]

	local export = false
	local static = false

	if specs.choice == "list" then
		for i=1, #specs do
			if specs[i].txt == "export" then
				if info.rule == "GlobalVariableDeclaration" then
					export = true
				else
					transpilerError("local variables cannot be exported")
				end
				export = true
			elseif specs[i].txt == "persistent" then
				if info.rule == "LocalVariableDeclaration" then
					static = true
				else
					transpilerError("global variables are persistent anyway")
				end
			end
		end
	end

	if info.rule == "GlobalVariableDeclaration" and not export then
		static = true
	end

	if info.rule == "GlobalVariableDeclaration" then
		local declaration = ""
		local definition = ""

		if #tparams == 0 then
			if export then
				declaration = "extern " .. const .. " rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. idtxt .. ";\n"
				codeSections.headerFunctionDeclarations = codeSections.headerFunctionDeclarations .. declaration
				definition = const .. " rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. idtxt .. " = " .. assigned.txt .. ";\n"
				codeSections.sourceFunctionDefinitions = codeSections.sourceFunctionDefinitions .. definition
			else
				definition = "static " .. const .. " rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. idtxt .. " = " .. assigned.txt .. ";\n"
				codeSections.sourceFunctionDefinitions = codeSections.sourceFunctionDefinitions .. definition
			end
		else
			local buf = ""
			if static then
				buf = "static "
			end
			declaration = concatTemplateParams(tparams, true) .. const ..
					" rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. idtxt .. ";\n"
			definition = concatTemplateParams(tparams, false) .. const ..
					" rem_cv_ref<decltype(" .. assigned.txt .. ")> " .. idtxt .. " = " .. assigned.txt .. ";\n"

			if export then
				codeSections.headerFunctionDeclarations = codeSections.headerFunctionDeclarations .. declaration
				codeSections.headerFunctionDefinitions = codeSections.headerFunctionDefinitions .. definition
			else
				codeSections.sourceFunctionDeclarations = codeSections.sourceFunctionDeclarations .. declaration
				codeSections.sourceFunctionDefinitions = codeSections.sourceFunctionDefinitions .. definition
			end
		end
	elseif info.rule == "LocalVariableDeclaration" then
		if #tparams > 0 then
			transpilerError("template variables cannot be declared in functions")
		end

		local definition = ""
		if static then
			definition = "static "
		end
		definition = definition .. const .. " auto " .. idtxt .. " = " .. assigned.txt .. ";\n"
		return {txt=definition}

	end

end
