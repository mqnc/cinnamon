
rule([[ CppExpression <- CppKeyword _ (Terminal/Empty) _ LBrace CppCode RBrace ]], basic.forward(6) )
table.insert(atomics, "CppExpression")
table.insert(keywords, "'C++'")

rule([[ CppMember <- CppKeyword _ MemberKeyword _ (Terminal/Empty) _ LBrace CppCode RBrace ]], basic.forward(8) )
rule([[ MemberKeyword <- "member" ]])

cppSectionDict={
	["hppTop"] = "headerHeader",
	["hppInclude"] = "headerIncludes",
	["hppUsing"] = "headerUsingDeclarations",
	["hppClassDeclaration"] = "headerClassForwardDeclarations",
	["hppFunctionDeclaration"] = "headerFunctionDeclarations",
	["hppVariableDeclaration"] = "headerFunctionDeclarations",
	["hppClassDefinition"] = "headerClassDeclarations",
	["hppFunctionDefinition"] = "headerFunctionDefinitions",
	["hppVariableDefinition"] = "headerFunctionDefinitions",
	["hppBottom"] = "headerFooter",
	["cppTop"] = "sourceHeader",
	["cppInclude"] = "sourceIncludes",
	["cppUsing"] = "sourceUsingDeclarations",
	["cppClassDeclaration"] = "sourceClassForwardDeclarations",
	["cppFunctionDeclaration"] = "sourceFunctionDeclarations",
	["cppVariableDeclaration"] = "sourceFunctionDeclarations",
	["cppClassDefinition"] = "sourceClassDeclarations",
	["cppFunctionDefinition"] = "sourceFunctionDefinitions",
	["cppVariableDefinition"] = "sourceFunctionDefinitions",
	["cppBottom"] = "sourceFooter"
}

rule([[ CppGlobal <- CppKeyword _ CppSection _ (Terminal/Empty) _ LBrace CppCode RBrace ]], function(sv, info)
	local section = sv[3].txt
	local code = sv[8].txt
	if cppSectionDict[section] == nil then
		transpilerError(section .. " is not a valid code section")
	else
		codeSections[cppSectionDict[section]] = codeSections[cppSectionDict[section]] .. "\n" .. code .. "\n"
	end
end)
table.insert(globalStatements, "CppGlobal")

rule([[ CppSection <- Identifier ]], basic.match )

rule([[ CppKeyword <- 'C++' ]])
rule([[ CppCode <- (CppComment / CppStringLiteral / CppScope / CppAnything)* ]], basic.match)
rule([[ CppComment <- CppLineEndComment / CppMultiLineComment ]])
rule([[ CppLineEndComment <- '//' (!'\n' .)* '\n' ]])
rule([[ CppMultiLineComment <- '/*' (!'*/' .)* '*/' ]])
rule([[ CppStringLiteral <- CppCharConstant / CppSimpleString / CppRawString ]])
rule([[ CppCharConstant <- '\'' (('\\' .) / .) '\'' ]])
rule([[ CppSimpleString <- '"' (('\\' .) / (!'"' .))* '"' ]])
rule([[ CppRawString <- 'R"' $delim<[a-zA-Z_0-9]*> '(' (!(')' $delim '"') .)* ')' $delim '"' ]])
rule([[ CppScope <- '{' CppCode '}' ]])
rule([[ CppAnything <- !'}' . ]])
