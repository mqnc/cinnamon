
rule([[ Scope <- ScopeKeyword _ Terminal ScopeBody EndScope ]], "{\n{4}\n}\n")
rule([[ ScopeKeyword <- "scope" ]], function(sv, info)
	table.insert(scopeStack, {})
	return {}
end)
table.insert(keywords, "Scope")
table.insert(localStatements, "Scope")

rule([[ ScopeInspector <- "@" ]], function(sv, info)
	dump(scopeStack)
	return {txt=""}
end )
table.insert(localStatements, "ScopeInspector")
table.insert(globalStatements, "ScopeInspector")

rule([[ ScopeBody <- FunctionBody ]], basic.forward(1) )

rule([[ EndScope <- EndKeyword ]], function(sv, info)
	table.remove(scopeStack)
	return {}
end )

rule([[ ImplicitEndScope <- '' ]], function(sv, info)
	table.remove(scopeStack)
	return {}
end )
