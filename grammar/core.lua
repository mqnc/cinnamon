--[[
This module provides the core features of the language:
syntax errors
line breaks
white spaces
comments
terminators
words
identifiers
]]

rule([[ Cinnamon <- Skip (GlobalStatement Skip)* ]], basic.concat )

-- syntax error has to be added to the list of statements after everything else
rule([[ SyntaxError <- (!NewLine .)* NewLine ]],  function(sv, info)
	transpilerError(info.line .. ":" .. info.column .. " Invalid Syntax: " .. basic.match(sv, info).txt)
	return {txt="ERROR"}
end )

rule([[ NewLine <- '\r\n' / '\n' ]], '\n')
rule([[ LineBreak <- LineEndComment? NewLine ]], basic.concat )
rule([[ Ellipsis <- '...' ]], ' ' )
rule([[ LineContinue <- Ellipsis _ LineBreak ]], basic.concat )
rule([[ WhiteSpace <- [ \t]+ ]], " " )
rule([[ Space <- (WhiteSpace / InlineComment / LineContinue)+ ]], basic.concat ) -- definite space
rule([[ _ <- Space? ]], basic.concat ) -- optional space
rule([[ Semicolon <- ';' ]], '')
rule([[ SilentTerminal <- LineBreak / Semicolon ]], basic.forward(1))
rule([[ Terminal <- SilentTerminal ]], ";{1}")

rule([[ Skip <- _ (LineBreak _)* ]], basic.concat ) -- consume all new lines and whitespaces (and comments)

--rule([[ Comment <- LineEndComment / InlineComment ]], basic.concat)
rule([[ LineEndComment <- '//' (!NewLine .)* ]], basic.match) -- does not include line break
rule([[ InlineComment <- MultiLineComment / NestableComment ]], basic.concat)
rule([[ MultiLineComment <- '/*' (!'*/' .)* '*/' ]], basic.match)
rule([[ NestableComment <- '(*' <(!'*)' (NestableComment / .))*> '*)' ]],
    function(sv, info)
		return {"/*" .. info.tokens[1]:gsub("/[*]", "(*"):gsub("[*]/", "*)") .. "*/"}
    end
)

rule([[ Word <- AsciiWord / Utf8Word ]], basic.forward(1))
rule([[ AsciiWord <- AsciiWordStart AsciiWordMid* WordEnd ]], basic.match)
rule([[ AsciiWordStart <- [a-zA-Z_] ]])
rule([[ AsciiWordMid <- [a-zA-Z_0-9] ]])
rule([[ WordEnd <- !Utf8WordMid ]], "")

rule([[ Utf8Word <- Utf8WordStart Utf8WordMid* WordEnd ]], function(sv, info)
	local match = basic.match(sv, info).txt

	local resulttxt = "u" -- so we never start with a number
	for i=1, #match do
		c = match:sub(i, i)
		local b = c:byte()

		if (b > 96) and (b <= 122) then
			resulttxt = resulttxt .. c
		else
			resulttxt = resulttxt .. string.format("%X", b)
		end
	end

	resulttxt = resulttxt .. "_" .. mark .. "_UTF8"

	return {txt=resulttxt}
end )
rule([[ Utf8WordStart <- [a-zA-Z_\u0080-\uFFFF] ]])
rule([[ Utf8WordMid <- [a-zA-Z_0-9\u0080-\uFFFF] ]])

rule([[ Identifier <- !Keyword Word ]], basic.forward(1) )
rule([[ IdentifierList <- Identifier (_ Comma _ Identifier)* ]], basic.listFilter )
rule([[ IdentifierListMulti <- Identifier _ Comma _ Identifier (_ Comma _ Identifier)* ]], basic.listFilter )

rule([[ Point <- '.' ]], '.' )
rule([[ Comma <- ',' ]], ',' )
rule([[ LParen <- '(' ]], '(' )
rule([[ RParen <- ')' ]], ')' )
rule([[ LBracket <- '[' ]], '[' )
rule([[ RBracket <- ']' ]], ']' )
rule([[ LBrace <- '{' ]], '{' )
rule([[ RBrace <- '}' ]], '}' )
rule([[ DoubleQuote <- '"' ]], '"' )
rule([[ Colon <- ':' ]], ':' )
rule([[ Asterisk <- '*' ]], '*' )
rule([[ DblColon <- '::' ]], '::' )

rule([[ SilentComma <- ',' ]], '' )
rule([[ SilentLParen <- '(' ]], '' )
rule([[ SilentRParen <- ')' ]], '' )
rule([[ SilentLBracket <- '[' ]], '' )
rule([[ SilentRBracket <- ']' ]], '' )
rule([[ SilentLBrace <- '{' ]], '' )
rule([[ SilentRBrace <- '}' ]], '' )

rule([[ InsertComma <- '' ]], ',' )
rule([[ InsertLParen <- '' ]], '(' )
rule([[ InsertRParen <- '' ]], ')' )
rule([[ InsertLBracket <- '' ]], '[' )
rule([[ InsertRBracket <- '' ]], ']' )
rule([[ InsertLBrace <- '' ]], '{' )
rule([[ InsertRBrace <- '' ]], '}' )

rule([[ Empty <- '' ]], function()
 	return {empty=true, txt=""}
end )

rule([[ EndKeyword <- 'end' ]])
table.insert(keywords, "EndKeyword")
