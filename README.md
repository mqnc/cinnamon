![Cinnamon logo](media/cinnamon_100p.png "Cinnamon")

# Usable Programming Language that Wraps C++

*in development, everything highly experimental*

One of the key paradigms of C++ is that the compiler reads everything only once so that the programmer has to write everything twice. Modern IDEs can do some of that work and [Lzz](https://www.lazycplusplus.com/) is amazing but I mean come on.

Also, C++ has grown over the years while staying backwards compatible as much as possible. Since inventing new keywords might break someone's code, every new language feature must have been a syntax error before. And this is exactly what C++ mostly looks like nowadays, just look at immediately-invoked lambdas.

The aim of Cinnamon is to provide a more usable programming language that covers most everyday use cases of most everyday people and translate that into C++ which can then be compiled into a lightning-fast program, just like normal C++.

# Examples

<table>
<tr><td colspan=2>&nbsp;<h4>Hello World!</h4></td></tr>
<tr><td>

Cinnamon

```cpp
function main
    println("Hello World!")
end
```

</td><td>

C++

```cpp
#include <iostream>

int main(){
    std::cout << "Hello World!\n";
}
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>Templated arguments and multiple return values</h4></td></tr>
<tr><td>

Cinnamon

```cpp
function swap(x, y)
    return y, x
end
```

</td><td>

C++

```cpp
template <typename T1, typename T2>
auto swap(const T1 x, const T2 y){
    return std::make_tuple(y, x);
}
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>Named return values</h4></td></tr>
<tr><td>

Cinnamon

```cpp
function divide(a:Int, b:Int) -> q:Int, r:Int
    return a div b, a mod b
end
```

</td><td>

C++

```cpp
struct DivisionResult{
    int q;
    int r;
};

DivisionResult divide(int a, int b){
    return {a / b, a % b};
}
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>Keyword arguments</h4></td></tr>
<tr><td>

Cinnamon

```cpp
function drawLine(...
        p0:Point, ...
        p1:Point, ...
        width := 1, ...
        color := BLACK, ...
        dashed := false, ...
        caps := SQUARE, ...
        join := BEVEL) | kwargs

    // do stuff using p0, p1 and so on

end

// call:
drawLine(p0, p1, caps := ROUND, join := ROUND)
```

</td><td>

C++ (from [rosettacode](https://rosettacode.org/wiki/Named_parameters#C.2B.2B))

```cpp
class LineParams{

    friend void drawLine(LineParams p);

public:

    LineParams(Point p0, Point p1):
        required_p0_m(p0),
        required_p1_m(p1),
        optional_width_m(1),
        optional_color_m(BLACK),
        optional_dashed_m(false),
        optional_caps_m(SQUARE),
        optional_join_m(BEVEL){}

    LineParams& width(int w){
        optional_width_m = w;
        return *this;
    }

    LineParams& color(Color c){
        optional_color_m = c;
        return *this;
    }

    LineParams& dashed(bool d){
        optional_dashed_m = d;
        return *this;
    }

    LineParams& caps(Caps c){
        optional_caps_m = c;
        return *this;
    }

    LineParams& join(Join j){
        optional_join_m = j;
        return *this;
    }

private:
    Point required_p0_m;
    Point required_p1_m;
    int optional_width_m;
    Color optional_color_m;
    bool optional_dashed_m;
    Caps optional_caps_m;
    Join optional_join_m;
};

void drawLine(LineParams p){

    // do stuff using p.p0, p.p1 and so on

}

// call:
drawLine(LineParams(p0, p1).caps(ROUND).join(ROUND))
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>Ranges and execution between iterations</h4></td></tr>
<tr><td>

Cinnamon<br>(implementing [Alexandrescu's philosophy](http://www.informit.com/articles/printerfriendly/1407357))

```cpp
// 1 2 3 4 5 6 7 8 9
for var i in [1..9]
    print(i, " ")
end

// 1 2 3 4 5 6 7 8 9
for var i in (0..10)
    print(i, " ")
end

// 1 2 3 4 5 6 7 8 9
for var i in [1..10)
    print(i, " ")
end

// 2, 4, 6, 8
for var i in (0..2..10)
    print(i)
between
    print(", ")
end
```

</td><td>

C++

```cpp
// 1 2 3 4 5 6 7 8 9
for(int i=1; i<=9; i++){
    std::cout << i << " ";
}

// 1 2 3 4 5 6 7 8 9
for(int i=1; i<10; i++){
    std::cout << i << " ";
}

// 1 2 3 4 5 6 7 8 9
for(int i=1; i<10; i++){
    std::cout << i << " ";
}

// 2, 4, 6, 8
bool first = true;
for(int i=2; i<10; i+=2){
    std::cout << i;
    if(!first){
        std::cout << ", ";
        first = false;
    }
}
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>Classes</h4></td></tr>
<tr><td>

Cinnamon

```cpp
class SpaceShip inherits Object

    const id := 0
    var shield := 0
    var ammo := 0
    (var) secretMission := "" // private
    [var] pilot := "" // protected

    ctor(id:Int, shield:Int, ammo:Int, ...
            mission:String, pilot:String)
        init
            self.id := id
            self.shield := shield
            self.ammo := ammo
            self.secretMission := ...
                    self.decrypt(mission)
            self.pilot := pilot
        end

        // stuff
    end

    // private method
    (method) decrypt(text:String)
        // stuff
    end

    // public method
    method update()
        // stuff
    end

    dtor
        // cleanup stuff
    end
end
```

</td><td>

C++

```cpp
// forward declaration
class SpaceShip;

class SpaceShip:public Object
{
public:
    SpaceShip(
        const int id,
        const int shield,
        const int ammo,
        const std::string mission,
        const std::string pilot
    );
    void update();
    ~SpaceShip();
private:
    void decrypt(const String text);

public:
    const int id = 0;
    int shield = 0;
    int ammo = 0;
private:
    std::string secretMission = "";
protected:
    std::string pilot = "";
};

SpaceShip::SpaceShip(
    const int id,
    const int shield,
    const int ammo,
    const std::string mission,
    const std::string pilot
):
    id{id},
    shield{shield},
    ammo{ammo},
    secretMission{
        decrypt(mission)
    },
    pilot{pilot}
{
    // stuff
}

void SpaceShip::decrypt(const String text)
{
    // stuff
}

void SpaceShip::update()
{
    // stuff
}

SpaceShip::~SpaceShip()
{
    // cleanup stuff
}
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>Modules</h4></td></tr>
<tr><td>

Cinnamon

```cpp
// lib.mon

function interface(i:Int, s:String) | export
    implementation()
end

function implementation()
    // stuff
end
```


```cpp
// main.mon

import lib
from otherlib import frobnicate as frob
from dirtylib import *

function main()
    lib::interface(1337, "covfefe")
    frob()
    hackery() // from dirtylib
end
```

</td><td>

C++

```cpp
// lib.hpp

#ifndef LIB_HPP
#define LIB_HPP

#include <string>

namespace lib{
    void interface(int i, std::string s);
}
#endif
```


```cpp
// lib.cpp

#include "lib.hpp"

namespace lib{
    void implementation();

    void interface(int i, std::string s){
        implementation();
    }

    void implementation(){
        // stuff
    }
}
```


```cpp
// main.cpp

#include "lib.hpp"
#include "otherlib.hpp"
#include "dirtylib.hpp"

using frob = otherlib::frobnicate;
using namespace dirtylib;

int main(){
    lib::interface(1337, "covfefe");
    frob();
    hackery(); // from dirtylib
    return 0;
}
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>The little things</h4></td></tr>
<tr><td>

Cinnamon

```cpp
class MyClass

    // multiple parameters for subscript
    method[param1, param2]
        // stuff
    end

end // no semicolon!!!

// main arguments are a std::vector
function main(args:DynArray)

    // there is no single "="
    var ready := power == 9000

    // bitwise operators have higher
    // precedence than comparisons
    if flags bitand mask == 0
        // stuff
    end

    // everything is always RAIId
    // and auto-typed (parameters can
    // have explicit type tho)
    var myObject := MyClass()

    // ";" and linebreak are equivalent
    var px := 1.4; var py := 3.1

    // tuples like in python
    var a, b := 7, 2
    var ab := a, b
    a, b := ab

    switch phase
        case PHASE1, PHASE2, PHASE3
            // break is implicit
        case CLEANUP
            // fallthrough is explicit
            fall
        case EXIT
            // exit stuff
        case default
            // default stuff
    end

    // custom infix operators
    // with custom precedence
    // via plugins
    var volume := a · b × c

    // unicode identifiers
    var ポケモン := "ピカチュウ"

end
```

</td><td>

C++

```cpp
// MyClass.hpp

class MyClass{

    void operator[](std::tuple<int, int> params){
        // stuff
    }

} // uh oh, forgot the semicolon
// compilers be like "something completely different
// is wrong in a completely unrelated file!"
```


```cpp
// main.cpp

// can't believe how c this is
int main(int argc, char *argv[]){

    if(power = 9000) // well I'm sure it is now
    {}

    if(flags & mask == 0){ // uh oh
        // this was (flags & (mask == 0))
    }

    MyClass myObject(); // uh oh
    // accidentally declared a function here

    auto px = 1.4, py = 3.1;
    // well that's actually ok

    auto [a, b] = std::make_tuple(7, 2);
    auto ab = std::make_tuple(a, b);
    std::tie(a, b) = ab;

    switch phase{
        case PHASE1: case PHASE2: case PHASE3:
            // uh oh, forgot the break
        case CLEANUP:
            // get a compiler warning
            // if I don't put [[fallthrough]]
        case EXIT:
            // exit stuff
        default:
            // default stuff
    end

    double volume = a.dot(b.cross(c));

    auto xenophobicLatinVarname = "ピカチュウ";
}
```

</td></tr>
<tr><td colspan=2>&nbsp;<h4>Anything you <i>really</i> like to do in C++</h4></td></tr>
<tr><td>

Cinnamon

```cpp
C++{goto batcave;}
```

</td><td>

C++

```cpp
goto batcave;
```

</td></tr>
</table>


## Requirements

For building, you need [SCons](https://scons.org/) and a C++ compiler. For experimenting with the grammar, I recommend [PEGDebug](https://github.com/mqnc/pegdebug). The tests are written in Python3.

## Building and Testing

```
sh build_parser.sh
python runtest.py
```

## How it works

At its core, Cinnamon uses a [PEG parser](https://en.wikipedia.org/wiki/Parsing_expression_grammar) written in C++ that is invoked from Lua. The Cinnamon grammar is itself generated in Lua and all C++-generating code that is invoked by the parser is also written in Lua.

## How you can use it

As a first try, create a folder named sandbox in the tests directory. In it, create a file with your program code and name it `main.mon`. Copy the source from another test if you're lazy. Then run `python runtest.py sandbox` which will transpile that into C++, compile it and run it.

## Why you should use it

- Main design goal of the language is syntactic diabetes so the code looks beautiful and is comprehensible (or easily googleable) for newbies. C++ lambdas shall serve as a negative example once again.

- The grammar for transpiling from Cinnamon to C++ is dynamically generated and you can modify it before transpiling. This allows for custom syntax (including custom operators (even infix!) with custom precedence and custom associativity).

- Most importantly: it automatically sets the semicolon after your class declaration!!!

## Why you shouldn't actually use it

- It is still in pre pre alpha prototype draft version. Although the aim is to cover up the C++ foot shotgun as well as possible, it is still there and maybe even more dangerous since it looks more innocent.

- Since this whole project is much more learning by doing than a well-designed piece of architecture, most parts of it are inconsistent proof-of-concept spaghetti code.

- If your code contains errors, best case scenario is that you get "There is some syntax error in this line." so in its current state, instead of making things easier, there is basically a fourth layer of annoyance on top of the usual C++ syntax errors, linker errors and bugs.

## Todos

- Smart pointers

- Include [this ranges library](https://bitbucket.org/jocki84/ranges/src/default/)

- Seamless integration of matrices (probably using eigen), physical units and arbitrary precision for yaw-dropping maths and physics computation

- Multithreading

- More expressive syntax errors

## How you can contribute

Try it out, spread the word, write your suggestions as issues!

## Credits

Working with [Julia](https://julialang.org/) demonstrated how pleasant it can be to work with a modern, well-designed programming language. It was a huge inspiration. If it was possible to compile Julia into small binaries, Cinnamon would not have been started.

[Python](https://www.python.org/) also influenced this project a lot and much of the syntax is just shamelessly copied.

Parsing is done using [cpp-peglib](https://github.com/yhirose/cpp-peglib) which works like a charm and its developer [yhirose](https://github.com/yhirose) reacted to feature requests like he doesn't have a life!

C++ code generation uses [Lua](https://www.lua.org/) which is a blazing fast scripting language that can be compiled into your own executables adding just a few hundred kb to the binary.

Building of the transpiler uses [SCons](https://scons.org/) because it's better than CMake.

A big thanks for utmost valuable discussions and contributions goes to [jkuebart](https://gitlab.com/jkuebart).

This project is awesomely supported by [Gestalt Robotics GmbH](https://www.gestalt-robotics.com/).
