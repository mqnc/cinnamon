
rule([[ Matrix <- ~LBracket ~Terminal? ~_ MatrixRow (~_ ~Terminal ~_ MatrixRow)* ~Terminal? ~_ ~RBracket ]], function(sv, info)
	local resulttxt = sv[1].txt
	for i=2, #sv do
		resulttxt = mark .. "_vcat(" .. resulttxt .. ", " .. sv[i].txt .. ")"
	end
	return {txt = resulttxt}
end )

table.insert(atomics, "Matrix")

rule([[ MatrixRow <- MatrixEntry (~_ ~Comma ~_ MatrixEntry)* ]], function(sv, info)
	local resulttxt = sv[1].txt
	for i=2, #sv do
		resulttxt = mark .. "_hcat(" .. resulttxt .. ", " .. sv[i].txt .. ")"
	end
	return {txt = resulttxt}
end )

rule([[ MatrixEntry <- Expression ]], mark .. "_matrix_wrap({1})")

for i=1, #OperatorClasses do
	if OperatorClasses[i].name == "Multiplication" then
		table.insert(OperatorClasses[i].operators, {peg="'dot' WordEnd", cpp="(({<}).dot({>}))"})
		table.insert(OperatorClasses[i].operators, {peg="'·'", cpp="(({<}).dot({>}))"})

		table.insert(OperatorClasses, i, {
			name = "CrossProduct",
			order = "ltr",
			operators = {
				{peg="'cross' WordEnd", cpp="(({<}).cross({>}))"},
				{peg="'×'", cpp="(({<}).cross({>}))"}
			}
		})

		break
	end
end

table.insert(keywords, "'dot'")
table.insert(keywords, "'cross'")
