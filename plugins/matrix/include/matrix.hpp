
#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <Eigen/Core>
#include <Eigen/Geometry>

//template<typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
//class Eigen::Matrix< _Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols >

namespace cinnamon{

	const auto Dynamic = Eigen::Dynamic;

	template <class T, int Rows, int Cols, class TiElem, std::enable_if_t<(Rows != Dynamic && Cols != Dynamic), int> = 0>
	auto m0n_invoke_subscript(Eigen::Matrix<T, Rows, Cols> m, TiElem iElem){
		return Eigen::Map<const Eigen::Matrix<T, Rows*Cols, 1> >(m.data(), m.size())(iElem);
	}

	template <class T, int Rows, int Cols, class TiElem, std::enable_if_t<(Rows == Dynamic || Cols == Dynamic), int> = 0>
	auto m0n_invoke_subscript(Eigen::Matrix<T, Rows, Cols> m, TiElem iElem){
		return Eigen::Map<const Eigen::Matrix<T, Dynamic, 1> >(m.data(), m.size())(iElem);
	}

	template <class T, int Rows, int Cols, class TiRow, class TiCol>
	auto m0n_invoke_subscript(Eigen::Matrix<T, Rows, Cols> m, TiRow iRow, TiCol iCol){
		return m(iRow, iCol);
	}

	template <class T, int Rows1, int Cols1, int Rows2, int Cols2,
			std::enable_if_t<(Rows1 != Dynamic && Cols1 != Dynamic && Rows2 != Dynamic && Cols2 != Dynamic), int> = 0>
	auto m0n_hcat(Eigen::Matrix<T, Rows1, Cols1> m1, Eigen::Matrix<T, Rows2, Cols2> m2){
		static_assert(Rows1 == Rows2);
		return (Eigen::Matrix<T, Rows1, Cols1+Cols2>() << m1, m2).finished();
	}

	template <class T, int Rows1, int Cols1, int Rows2, int Cols2,
			std::enable_if_t<(Rows1 == Dynamic || Cols1 == Dynamic || Rows2 == Dynamic || Cols2 == Dynamic), int> = 0>
	auto m0n_hcat(Eigen::Matrix<T, Rows1, Cols1> m1, Eigen::Matrix<T, Rows2, Cols2> m2){
		assert(m1.rows() == m2.rows());
		return (Eigen::Matrix<T, Dynamic, Dynamic>(m1.rows(), m1.cols()+m2.cols()) << m1, m2).finished();
	}

	template <class T, int Rows1, int Cols1, int Rows2, int Cols2,
			std::enable_if_t<(Rows1 != Dynamic && Cols1 != Dynamic && Rows2 != Dynamic && Cols2 != Dynamic), int> = 0>
	auto m0n_vcat(Eigen::Matrix<T, Rows1, Cols1> m1, Eigen::Matrix<T, Rows2, Cols2> m2){
		static_assert(Cols1 == Cols2);
		return (Eigen::Matrix<T, Rows1+Rows2, Cols1>() << m1, m2).finished();
	}

	template <class T, int Rows1, int Cols1, int Rows2, int Cols2,
			std::enable_if_t<(Rows1 == Dynamic || Cols1 == Dynamic || Rows2 == Dynamic || Cols2 == Dynamic), int> = 0>
	auto m0n_vcat(Eigen::Matrix<T, Rows1, Cols1> m1, Eigen::Matrix<T, Rows2, Cols2> m2){
		assert(m1.cols() == m2.cols());
		return (Eigen::Matrix<T, Dynamic, Dynamic>(m1.rows()+m2.rows(), m1.cols()) << m1, m2).finished();
	}

	/*
	template <typename T>
	T m0n_get_scalar(T t){return t;}

	template <typename T, int Rows, int Cols>
	typename Eigen::Matrix<T, Rows, Cols>::Scalar m0n_get_scalar(Eigen::Matrix<T, Rows, Cols> t){return t;}
	*/

	template <class T>
	auto m0n_matrix_wrap(T val){
		return (Eigen::Matrix<T, 1, 1>() << val).finished();
	}

	template <class T=double, int Rows=Dynamic, int Cols=Dynamic>
	auto m0n_matrix_wrap(Eigen::Matrix<T, Rows, Cols> mtx){
		return mtx;
	}

	template <class T=double, int Rows=Dynamic, int Cols=Dynamic>
	using Matrix = Eigen::Matrix<T, Rows, Cols>;

	template <class T=double, int Cols=Dynamic>
	using RowVector = Eigen::Matrix<T, 1, Cols>;

	template <class T=double, int Rows=Dynamic>
	using ColumnVector = Eigen::Matrix<T, Rows, 1>;


}

#endif
