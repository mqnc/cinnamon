
#include "C:\Users\mikunze\home\development/cinnamon/plugins/matrix/include/Eigen/Eigen"

class DDOT{};
class LBRACKET{};
class RBRACKET{};
class BREAK{};
class EMPTY{};

const auto Dynamic = Eigen::Dynamic;

template <class T, int Rows1, int Cols1, int Rows2, int Cols2>
auto hcat(Eigen::Matrix<T, Rows1, Cols1> m1, Eigen::Matrix<T, Rows2, Cols2> m2){
	static_assert(Rows1 == Rows2);
	return (Eigen::Matrix<T, Rows1, Cols1+Cols2>() << m1, m2).finished();
}

template <class T, int Rows1, int Cols1>
auto hcat(Eigen::Matrix<T, Rows1, Cols1> m1, EMPTY){
	return m1;
}

template <class T, int Rows1, int Cols1, int Rows2, int Cols2>
auto vcat(Eigen::Matrix<T, Rows1, Cols1> m1, Eigen::Matrix<T, Rows2, Cols2> m2){
	static_assert(Cols1 == Cols2);
	return (Eigen::Matrix<T, Rows1+Rows2, Cols1>() << m1, m2).finished();
}

template <class T, int Rows1, int Cols1>
auto vcat(Eigen::Matrix<T, Rows1, Cols1> m1, EMPTY){
	return m1;
}

template <class T>
auto matrixWrap(T val){
	return (Eigen::Matrix<T, 1, 1>() << val).finished();
}

template <class T=double, int Rows=Dynamic, int Cols=Dynamic>
auto matrixWrap(Eigen::Matrix<T, Rows, Cols> mtx){
	return mtx;
}

template <typename T, typename... Ts>
auto matrixRestRow(T value, Ts... rest){
	return hcat(matrixWrap(value), matrixRestRow(rest...));
}

template <typename... Ts>
auto matrixRestRow(BREAK, Ts... rest){
	return EMPTY();
}

auto matrixRestRow(RBRACKET){
	return EMPTY();
}

template <typename... Ts>
auto matrixNextRows(BREAK, Ts... rest){
	return matrixRows(rest...);
}

auto matrixNextRows(RBRACKET){
	return EMPTY();
}

template <typename T, typename... Ts>
auto matrixNextRows(T, Ts... rest){
	return matrixNextRows(rest...);
}

template <typename T, typename... Ts>
auto matrixRows(T value, Ts... rest){
	return vcat(
		hcat(matrixWrap(value), matrixRestRow(rest...)),
		matrixNextRows(rest...)
	);
}

template <typename... Ts>
auto matrix(LBRACKET, Ts... rest){
	return matrixRows(rest...);
}


int main(){

	cout << matrix(LBRACKET(), 1, 2, BREAK(), 3, 4, BREAK(), 5, 6, RBRACKET());

	return 0;
}
