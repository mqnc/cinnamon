

/*
this contains the first idea for implementing references
before it dawned on me that I could just use normal c++
references for everything
*/


#include "canapply.hpp"

template <class T>
using supports_contains_refs_t = decltype( std::declval<T>().contains_refs_m() );
template <class T>
using supports_contains_refs = can_apply<supports_contains_refs_t, T>;

template <class T, std::enable_if_t<supports_contains_refs<T>{}, int> = 0>
constexpr bool contains_refs(T obj){
	return obj.contains_refs_m();
}

template <class T, std::enable_if_t<!supports_contains_refs<T>{}, int> = 0>
constexpr bool contains_refs(T obj){
	return false;
}

template <class T, std::enable_if_t<supports_contains_refs<T>{}, int> = 0>
constexpr bool contains_refs(){
	return T::contains_refs_m();
}

template <class T, std::enable_if_t<!supports_contains_refs<T>{}, int> = 0>
constexpr bool contains_refs(){
	return false;
}


template <class T>
class Referable{
public:
	Referable(T value):value_m(value){}

	constexpr auto& operator *()
	{
		return value_m;
	}

	static constexpr bool contains_refs_m()
	{
		return contains_refs<T>();
	}

private:
	T value_m;
};


template <class T>
class Reference{
public:
	Reference(Referable<T>& target):ref_value_m(*target){}
	Reference(Reference<T>& target):ref_value_m(*target){}

	constexpr auto& operator *()
	{
		return ref_value_m;
	}

	static constexpr bool contains_refs_m()
	{
		return true;
	}

private:
	T& ref_value_m;
};
