#include <array>
#include <cstdint>
//#include <deque>
//#include <iomanip>
#include <iostream>
//#include <memory>
//#include <sstream>
#include <string>
#include <tuple>
//#include <type_traits>
//#include <utility>
#include <vector>

using namespace std;

int main(int argc, char const *argv[]) {
	vector<string> args(argv, argv + argc);

	int x=0, y=0, z=0, w=0;
	tuple<int, int> yz;

	tuple<int&, tuple<int&, int&>, int&>{x, {y, z}, w} = make_tuple(4, make_tuple(5, 6), 7);

	auto [h, ij, k] = make_tuple(4, make_tuple(5, 6), 7);
	auto& [i, j] = ij;

	cout << x << y << z << w;

	return 0;
}
