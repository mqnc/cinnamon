
#include "tag.hpp"
#include "tuple_tools.hpp"
#include <tuple>
#include <cassert>

using namespace std;

// value not found
template<int Idx, class Tag, int This, class Tup, class Enable=void>
struct scan_impl{
	static constexpr int value = -1;
};

// we're at the desired index and we're at a positional argument
template<int IdxIsThis, class Tag, class Head, class... Tail>
struct scan_impl<IdxIsThis, Tag, IdxIsThis, tuple<Head, Tail...>, enable_if_t<!is_tagged_v<Head>> >{
	static constexpr int value = IdxIsThis;
};

// we found the desired tag
template<int Idx, class TagIsHead, int This, class HeadValue, class... Tail>
struct scan_impl<Idx, TagIsHead, This, tuple<Tagged<TagIsHead, HeadValue>, Tail...> >{
	static constexpr int value = This;
};

// positional but wrong index, move on
template<int Idx, class Tag, int This, class Head, class... Tail>
struct scan_impl<Idx, Tag, This, tuple<Head, Tail...>,
		enable_if_t<Idx != This && !is_tagged_v<Head> > >:
	scan_impl<Idx, Tag, This+1, tuple<Tail...> >
{};

// tagged but wrong tag, move on
template<int Idx, class Tag, int This, class HeadTag, class HeadValue, class... Tail>
struct scan_impl<Idx, Tag, This, tuple<Tagged<HeadTag, HeadValue>, Tail...> >:
	scan_impl<Idx, Tag, This+1, tuple<Tail...> >
{};

// wrapper
template<int Idx, class Tag, class Tup>
inline constexpr int scan = scan_impl<Idx, Tag, 0, Tup>::value;



template <int... ints>
struct indices{};



template <typename IS>
struct print;

template <int... Ints>
struct print<indices<Ints...> >{
	print(){
		(cout << Ints << " ", ...);
	}
};



template <int i, typename IS>
struct contains_impl;

template <int i, int... ints>
struct contains_impl<i, indices<i, ints...> >:true_type{};

template <int i, int i0, int... ints>
struct contains_impl<i, indices<i0, ints...> >:
	contains_impl<i, indices<ints...> >
{};

template <int i>
struct contains_impl<i, indices<> >:false_type{};

template <int i, typename IS>
inline constexpr bool contains = contains_impl<i, IS>::value;



template <class IS, int i>
struct append;

template <int... ints, int i>
struct append<indices<ints...>, i>{
	typedef indices<ints..., i> type;
};



template <int n, typename IS>
struct rest{
	typedef conditional_t<
		contains<n-1, IS>,
		typename rest<n-1, IS>::type,
		typename append<typename rest<n-1, IS>::type, n-1>::type
	> type;
};

template <typename IS>
struct rest<0, IS>{
	typedef indices<> type;
};


template <typename Mapping, typename Tup>
struct map_impl{};

template <int... Ints, typename Tup>
struct map_impl<indices<Ints...>, Tup>{
	static constexpr auto apply(Tup args){
		return make_tuple(get<Ints>(args)...);
	}
};

template <typename Mapping, typename Tup>
auto map(Tup tup){
	return map_impl<Mapping, Tup>::apply(tup);
}



template <typename Tup>
auto f(Tup tup){
	typedef indices<
		scan<0, Name<'a', '0'>, Tup>,
		scan<1, Name<'a', '1'>, Tup>,
		scan<2, Name<'a', '2'>, Tup>,
		scan<3, Name<'a', '3'>, Tup>,
		scan<4, Name<'a', '4'>, Tup>,
		scan<5, Name<'a', '5'>, Tup>,
		scan<6, Name<'a', '6'>, Tup>
	> mapping;
	typedef typename rest<7, mapping>::type remaining;

	auto [a0, a1, a2, a3, a4, a5, a6] = map<mapping>(tup);
	auto other = map<remaining>(tup);

	cout << a0 << ", " << a1 << ", " << a2 << ", " << a3 << ", " << a4 << ", " << a5 << ", " << a6 << '\n';
	cout << other << '\n';
}


int main(int argc, char const *argv[]) {

	auto args = make_tuple(0, 1, 2, 3, tag<Name<'r'> >(77), tag<Name<'a','6'> >(6), tag<Name<'a','4'> >(4), tag<Name<'a','5'> >(5));

	f(args);

	//cout << contains<5, indices<0, 21, 3, 9, 7, 5> >;

	//conditional<false, int, double>::type x = 5.5;

	//cout << x;

	//print<rest<10, indices<90, 1,41,2,4,6> >::type >();


	auto scantest = make_tuple(0, 1, tag<Name<'a'> >("a"), tag<Name<'b'> >("b"));
	assert((scan<0, Name<'x'>, decltype(scantest)> == 0));
	assert((scan<1, Name<'x'>, decltype(scantest)> == 1));
	assert((scan<2, Name<'x'>, decltype(scantest)> == -1));
	assert((scan<9, Name<'a'>, decltype(scantest)> == 2));
	assert((scan<9, Name<'b'>, decltype(scantest)> == 3));
	assert((scan<9, Name<'x'>, decltype(scantest)> == -1));

	return 0;
}
