
#include <iostream>
#include <tuple>
#include <cassert>

using namespace std;

  //////////
 // Tags //
//////////

// for string tags, will hopefully be replaced by template<string S> in c++20
template <char... Cs>
struct Name{
	static const string str(){
		return (string("") + ... + Cs);
	}
};

// tagged value
template <typename Tag, typename Val>
class Tagged{
public:
	Val value;
};

// convenience function to create a tagged value
template <typename Tag, typename Val>
auto tag(Val value){
	return Tagged<Tag, Val>{value};
}

// printing a tagged value
template<typename Tag, typename Val>
ostream& operator<<(ostream& os, Tagged<Tag, Val> tagged){
	os << "<" << Tag::str() << ":" << tagged.value << ">";
	return os;
}


  ////////////
 // Tuples //
////////////

template <int... Ints>
struct indices{
	static const string str(){
		return (string("") + ... + (to_string(Ints) + " "));
	}
};

// printing a tuple
template<typename... Ts>
ostream& operator<<(ostream& os, tuple<Ts...> tup){
	os << "( ";
	std::apply([&](auto&... vals){
		((os << vals << " "), ...);
	}, tup);
	os << ")";
	return os;
}

template <typename T, typename Tup>
struct prepend;

template <typename T, typename... Ts>
struct prepend<T, tuple<Ts...> >{
	typedef tuple<T, Ts...> type;
};



template <typename Tup>
struct get_positional;

template <typename Pos, typename... Tail>
struct get_positional<tuple<Pos, Tail...> >{
	typedef typename prepend<
		Pos,
		typename get_positional<tuple<Tail...>>::type
	>::type type;
};

template <typename Tag, typename Val, typename... Tail>
struct get_positional<tuple<Tagged<Tag, Val>, Tail...> >{
	typedef tuple<> type;
};

template <>
struct get_positional<tuple<> >{
	typedef tuple<> type;
};

template <typename... Args>
using get_positional_t = typename get_positional<Args...>::type;


template <typename Tup>
struct get_tagged;

template <typename Pos, typename... Tail>
struct get_tagged<tuple<Pos, Tail...> >{
	typedef typename get_tagged<tuple<Tail...>>::type type;
};

template <typename Tag, typename Val, typename... Tail>
struct get_tagged<tuple<Tagged<Tag, Val>, Tail...> >{
	typedef typename prepend<
		Tagged<Tag, Val>,
		typename get_tagged<tuple<Tail...>>::type
	>::type type;
};

template <>
struct get_tagged<tuple<> >{
	typedef tuple<> type;
};

template <typename... Args>
using get_tagged_t = typename get_tagged<Args...>::type;



template <typename Tup, int Idx>
struct get_idx{
	static constexpr int value = tuple_size_v<Tup> > Idx ? Idx : -1;
};

template <typename Tup, typename Tag, int Idx>
struct find_tag;

template <typename Tag, typename Val, int Idx, typename... Ts>
struct find_tag<tuple<Tagged<Tag, Val>, Ts...>, Tag, Idx>{
	static constexpr int value = Idx;
};

template <typename Other, typename Val, typename Tag, int Idx, typename... Ts>
struct find_tag<tuple<Tagged<Other, Val>, Ts...>, Tag, Idx>:
	find_tag<tuple<Ts...>, Tag, Idx+1>{};

template <typename Tag, int Idx>
struct find_tag<tuple<>, Tag, Idx>{
	static constexpr int value = -1;
};



template <typename PosTup, int Idx, typename TagTup, typename Tag>
struct get_arg_idx{
private:
	static constexpr int posIdx = get_idx<PosTup, Idx>::value;
	static constexpr int tagIdx = find_tag<TagTup, Tag, tuple_size_v<PosTup>>::value;
	static_assert( posIdx == -1 || tagIdx == -1, "positional argument can not appear as named argument again" );
public:
	static constexpr int value = posIdx > -1 ? posIdx : tagIdx;
};

template <typename PosTup, int Idx, typename TagTup, typename Tag>
inline constexpr int get_arg_idx_v = get_arg_idx<PosTup, Idx, TagTup, Tag>::value;





template <typename Idcs, int Idx>
struct contains;

template <int... Tail, int Idx>
struct contains<indices<Idx, Tail...>, Idx>:true_type{};

template <int Head, int... Tail, int Idx>
struct contains<indices<Head, Tail...>, Idx>:
	contains<indices<Tail...>, Idx >
{};

template <int Idx>
struct contains<indices<>, Idx>:false_type{};

template <typename Idcs, int Idx>
inline constexpr bool contains_v = contains<Idcs, Idx>::value;




template <typename Idcs, int Idx>
struct append;

template <int... Ints, int Idx>
struct append<indices<Ints...>, Idx>{
	typedef indices<Ints..., Idx> type;
};



template <int Len, typename RmIdcs>
struct remaining{
	typedef conditional_t<
		contains_v<RmIdcs, Len-1>,
		typename remaining<Len-1, RmIdcs>::type,
		typename append<typename remaining<Len-1, RmIdcs>::type, Len-1>::type
	> type;
};

template <typename RmIdcs>
struct remaining<0, RmIdcs>{
	typedef indices<> type;
};

template <int Len, typename RmIdcs>
using remaining_t = typename remaining<Len, RmIdcs>::type;




template <typename Mapping, typename Args, typename Dflts, int Idx=0>
struct map_impl;

template <typename Args, typename Dflts, int Idx>
struct map_impl <indices<>, Args, Dflts, Idx>
{
	static auto get(Args, Dflts){
		return make_tuple();
	}
};

template <int MapHead, int... MapTail, typename Args, typename Dflts, int Idx>
struct map_impl< indices<MapHead, MapTail...>, Args, Dflts, Idx>
{
	static auto get(Args args, Dflts dflts){
		if constexpr(MapHead >= 0){
			return tuple_cat(
				make_tuple(std::get<MapHead>(args)),
				map_impl<indices<MapTail...>, Args, Dflts, Idx+1>::get(args, dflts)
			);
		}
		else{
			return tuple_cat(
				make_tuple(std::get<Idx>(dflts)),
				map_impl<indices<MapTail...>, Args, Dflts, Idx+1>::get(args, dflts)
			);
		}
	}
};

template <typename Mapping, typename Args, typename Dflts>
auto map(Args args, Dflts dflts){
	return map_impl<Mapping, Args, Dflts>::get(args, dflts);
}


template <typename Tup>
auto f(Tup args){
	auto [a, b, c, d, rest] = [&args](){
		typedef get_positional_t<Tup> posArgs;
		typedef get_tagged_t<Tup> tagArgs;

		typedef indices<
			get_arg_idx_v<posArgs, 0, tagArgs, Name<'a'> >,
			get_arg_idx_v<posArgs, 1, tagArgs, Name<'b'> >,
			get_arg_idx_v<posArgs, 2, tagArgs, Name<'c'> >,
			get_arg_idx_v<posArgs, 3, tagArgs, Name<'d'> >
		> mapping;

		typedef remaining_t<tuple_size_v<Tup>, mapping> appendix;

		return tuple_cat(
			map<mapping>(args, make_tuple("(0)", "(1)", "(2)", "(3)")),
			make_tuple( map<appendix>(args, make_tuple()) )
		);
	}();

	cout << a << ',' << b << ',' << c << ',' << d << ',' << rest;
}


int main(int argc, char const *argv[]) {

	f(make_tuple());
	cout << '\n';
	f(make_tuple(0, 1.1, tag<Name<'c'> >("c"), tag<Name<'r'> >("r"), tag<Name<'d'> >("d")));
	cout << '\n';
	f(make_tuple(0, 1, 2, 3, 4, 5, 6, 7));

	return 0;
}
