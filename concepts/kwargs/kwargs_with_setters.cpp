// g++ -o kwargs2 -std=c++17 kwargs2.cpp && ./kwargs2 && rm kwargs2

#include <iostream>
#include <tuple>

template <int I, typename... Ts>
decltype(auto) get(Ts&&... ts){
	return std::get<I>(std::forward_as_tuple(ts...));
}

/*

function(a, b, c, d=3, e=5)

*/


class fun{
	int a;
	int b;
	int c;
	int d=3;
	int e=5;

public:
	template <typename... Ts>
	fun(Ts&&... ts){
		if constexpr(sizeof...(ts) > 0){a = get<0>(ts...);}
		if constexpr(sizeof...(ts) > 1){b = get<1>(ts...);}
		if constexpr(sizeof...(ts) > 2){c = get<2>(ts...);}
		if constexpr(sizeof...(ts) > 3){d = get<3>(ts...);}
		if constexpr(sizeof...(ts) > 4){e = get<4>(ts...);}
	}

	fun& set_a(int _a){a = _a; return *this;}
	fun& set_b(int _b){b = _b; return *this;}
	fun& set_c(int _c){c = _c; return *this;}
	fun& set_d(int _d){d = _d; return *this;}
	fun& set_e(int _e){e = _e; return *this;}

	void invoke(){
		std::cout << "a=" << a << ", b=" << b << ", c=" << c << ", d=" << d << ", e=" << e << "\n";
	}
};


int main(){
	using u = fun;

	u(1, 2).set_d(4).set_e(5).invoke();
	return 0;
}
