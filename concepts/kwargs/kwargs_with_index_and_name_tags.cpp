
#include <iostream>
#include <string>
#include <vector>

using namespace std;


// for string tags, will hopefully be replaced by template<string S> in c++20
template <char... Cs>
struct Name{};


// tagged value
template <typename Key, typename Val>
class Tagged{
public:
	Val value;
};

// convenience function to create a tagged value
template <typename Key, typename Val>
auto tag(Val value){
	return Tagged<Key, Val>{value};
}

// for checking if an object is a tagged value
template<typename T>
struct is_tagged: public std::false_type {};

template<typename Key, typename Val>
struct is_tagged<Tagged<Key, Val> > : public std::true_type {};

// can be fed to cout
template<typename Key, typename Val>
ostream& operator<<(ostream& os, Tagged<Key, Val> t){
	os << "<" << t.value << ">";
	return os;
}


// very handy empty class
// TODO: this type must be hidden, it may not be used from the outside on purpose
struct Empty{
	const string value = "(empty)"; // for test purposes
};

// can be fed to cout
ostream& operator<<(ostream& os, Empty e){
	os << e.value;
	return os;
}



// extract<i>(...) extracts the ith argument and returns empty if a tagged argument appears before or not enough arguments are provided
// extract<key_t>(...) returns the only! argument that is tagged with the type key_t or empty if that key is not found

// empty argument list, return empty
template <size_t I>
auto extract(){
	return Empty();
}

// return the first argument in the list if it's not tagged and I==0, otherwise advance one argument
template <size_t I, typename T, typename... Ts,
		enable_if_t<!is_tagged<T>::value, int> = 0>
auto extract(T arg, Ts... rest){
	if constexpr(I == 0){
		return arg;
	}
	else{
		return extract<I-1>(rest...);
	}
}

// we are looking for a positional argument but the first one in the list is tagged, return empty
template <size_t I, typename Key=Empty, typename Val=Empty, typename... Ts>
auto extract(Tagged<Key, Val> arg, Ts... rest){
	return Empty();
}

// empty argument list, return empty
template <typename Query>
auto extract(){
	return Empty();
}

// if the first argument in the list is tagged with the key that we search for, return it
template <typename Query, typename Key, typename Val, typename... Ts,
		enable_if_t<is_same<Query, Key>::value, int> = 0>
auto extract(Tagged<Key, Val> match, Ts... rest){
	static_assert(is_same<decltype(extract<Query>(rest...)), Empty>::value, "named argument provided multiple times");
	return match;
}

// if the first argument in the list is NOT tagged with the key that we search for, move on
template <typename Query, typename Key, typename Val, typename... Ts,
		enable_if_t<!is_same<Query, Key>::value, int> = 0>
auto extract(Tagged<Key, Val> nomatch, Ts... rest){
	return extract<Query>(rest...);
}

// we are looking for a tagged argument but the first one in the list is positional, move on
template <typename Query, typename T, typename... Ts,
		enable_if_t<!is_tagged<T>::value, int> = 0>
auto extract(T pos, Ts... rest){
	return extract<Query>(rest...);
}

// select the positional argument or the tagged argument or if those are empty the default value
// fail if both positional and tagged argument are non-empty
template <typename Tpos, typename Tdflt=Empty>
auto arg_select(Tpos pos, Empty, Tdflt dflt=Empty()){return pos;}

template <typename Ttag, typename Tdflt=Empty>
auto arg_select(Empty, Ttag tagged, Tdflt dflt=Empty()){return tagged.value;}

template <typename Tdflt>
auto arg_select(Empty, Empty, Tdflt dflt){return dflt;}

// retreive value from arguments by position or name, fail if both work
template <size_t Idx, typename Tag, typename... Ts>
auto arg_get(Ts... args){
	return arg_select(
		extract<Idx>(args...),
		extract<Tag>(args...)
	);
}

// same as above but provide default if the others are empty
template <size_t Idx, typename Tag, typename Tdflt, typename... Ts>
auto arg_get_dflt(Tdflt dflt, Ts... args){
	return arg_select(
		extract<Idx>(args...),
		extract<Tag>(args...),
		dflt
	);
}

// f(a0, a1="a1", a2, rest*)
template <typename... Ts>
void f(Ts... args){
	auto a0 = arg_get<0, Name<'a','0'>>(args...);
	auto a1 = arg_get_dflt<1, Name<'a','1'>>("a1", args...);
	auto a2 = arg_get<2, Name<'a','2'>>(args...);
	auto a3 = arg_get<3, Name<'a','3'>>(args...);

	cout << a0 << a1 << a2 << a3;
}


int main(int argc, char const *argv[]) {
	vector<string> args(argv, argv + argc);

	f(
		"a0",
		"A1",
		tag<Name<'a', '3'> >("a3"),
		tag<Name<'a', '2'> >("a2")
	);

	return 0;
}
