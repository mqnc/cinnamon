
#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <char... Cs>
struct Name{};

template <size_t I>
struct Idx{};

template <typename K, typename V>
class Tagged{
public:
	V value;
};

struct Empty{
	const string value = "(empty)"; // for test purposes
};

template <typename K, typename V>
auto tag(V value){
	return Tagged<K, V>{value};
}

template <typename Query>
auto extract(){
	return Empty();
}

template <typename Query, typename Key, typename Val, typename... Ts>
struct extractor{
	static auto apply(Tagged<Key, Val> nomatch, Ts... rest){
		return extract<Query>(rest...);
	}
};

template <typename Key, typename Val, typename... Ts>
struct extractor<Key, Key, Val, Ts...>{
	static auto apply(Tagged<Key, Val> match, Ts... rest){
		return match;
	}
};

template <typename Query, typename Key, typename Val, typename... Ts>
auto extract(Tagged<Key, Val> test, Ts... rest){
	return extractor<Query, Key, Val, Ts...>::apply(test, rest...);
}


int main(int argc, char const *argv[]) {
	vector<string> args(argv, argv + argc);

	cout << extract< Idx<0> >(
		tag<Idx<0> >("pos arg 0"),
		tag<Name<'t','e','s','t'> >("named arg 'test'")
	).value << "\n";

	cout << extract< Name<'t','e','s','t'> >(
		tag<Idx<0> >("pos arg 0"),
		tag<Name<'t','e','s','t'> >("named arg 'test'")
	).value << "\n";

	cout << extract< Name<'t','e','s','t','2'> >(
		tag<Idx<0> >("pos arg 0"),
		tag<Name<'t','e','s','t'> >("named arg 'test'")
	).value << "\n";

	return 0;
}
