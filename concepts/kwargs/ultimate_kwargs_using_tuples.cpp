
#include "tag.hpp"
#include "tuple_tools.hpp"

using namespace std;

template <bool requireTagged=false, typename Tup>
auto assertOrder(Tup tup){
	if constexpr(tuple_size_v<Tup> > 0){
		constexpr bool headTagged = is_tagged_v<tuple_element_t<0,Tup> >;
		static_assert(!requireTagged || headTagged, "positional argument appearing after tagged argument");
		assertOrder<requireTagged || headTagged>(tuple_rmfirst(tup));
	}
}

template <typename Tup>
auto getPositional(Tup tup){
	if constexpr(tuple_size_v<Tup> == 0){
		return make_tuple();
	}
	else{
		constexpr bool headTagged = is_tagged_v<tuple_element_t<0,Tup> >;
		if constexpr(headTagged){
			return make_tuple();
		}
		else{
			return tuple_prepend(tuple_first(tup), getPositional(tuple_rmfirst(tup)));
		}
	}
}

template <typename Tup>
auto getTagged(Tup tup){
	if constexpr(tuple_size_v<Tup> == 0){
		return make_tuple();
	}
	else{
		constexpr bool headTagged = is_tagged_v<tuple_element_t<0,Tup> >;
		if constexpr(headTagged){
			return tup;
		}
		else{
			return getTagged(tuple_rmfirst(tup));
		}
	}
}

template <typename Tup1, typename Tup2>
auto joinArgs(Tup1 tup1, Tup2 tup2){
	auto result = tuple_cat(tup1, tup2);
	assertOrder(result);
	return result;
}

template <typename Tag, typename Tup>
auto extract(Tup tup){
	if constexpr(tuple_size_v<Tup> == 0){
		return make_pair(
			make_tuple(),
			make_tuple()
		);
	}
	else{
		auto [findings, rest] = extract<Tag>(tuple_rmfirst(tup));
		auto head = tuple_first(tup);
		if constexpr(is_same_v<Tag, typename decltype(head)::Tag>){
			return make_pair(
				tuple_prepend(head, findings),
				rest
			);
		}
		else{
			return make_pair(
				findings,
				tuple_prepend(head, rest)
			);
		}
	}
}


template <size_t i, typename Tag, auto dflt, typename AL>
auto filter(AL argLists){
	auto [sorted, positionals, tagged] = argLists;

	if constexpr(i < tuple_size_v<decltype(positionals)>){
		return make_tuple(
			tuple_prepend(get<i>(positionals), sorted),
			tuple_remove<i>(positionals),
			tagged
		);
	}
	else{
		auto [findings, rest] = extract<Tag>(tagged);
		if constexpr(tuple_size_v<decltype(findings)> == 1){
			return make_tuple(
				tuple_cat(findings, sorted),
				positionals,
				rest
			);
		}
		else{
			return make_tuple(
				tuple_prepend(dflt, sorted),
				positionals,
				rest
			);
		}
	}
}


int main(int argc, char const *argv[]) {

	auto args = make_tuple(0, 1, 2, 3, 100, 200, 300, 400, tag<Name<'a','4'> >(4), tag<Name<'a','5'> >(5), tag<Name<'a','6'> >(6));

	auto [sorted, positionalRest, taggedRest] =
		filter<0, Name<'a', '0'>, -1 >(
		filter<1, Name<'a', '1'>, -1 >(
		filter<2, Name<'a', '2'>, -1 >(
		filter<3, Name<'a', '3'>, -1 >(
		filter<4, Name<'a', '4'>, -1 >(
			make_tuple(
				make_tuple(), // sorted args
				getPositional(args),
				getTagged(args)
			)
		)))));

	cout << "sorted:" << sorted << '\n';
	cout << "positionalRest:" << positionalRest << '\n';
	cout << "taggedRest:" << taggedRest << '\n';

	assertOrder(args);

	return 0;
}
