

// flag that this call uses keyword arguments
class KWARGS{};

// marker for positional arguments
template <size_t I>
class POS{};

// list of all keyword argument markers in this namespace
// will be replaced by
//     template <string S>
//     class KW{};
// in cpp 20
class KW_a{};
class KW_x{};
class KW_y{};

// KW_extract extracts an argument which comes after either the specified
// positional marker or the specified named marker
template <typename P, typename KW, typename K, typename T, typename... Ts,
	enable_if_t<is_same<P, K>::value || is_same<KW, K>::value, int> = 0>
auto KW_extract(K, T value, Ts... args){
	return value;
}

// not a match, move on to the next marker/value pair
template <typename P, typename KW, typename K, typename T, typename... Ts,
	enable_if_t<!is_same<P, K>::value && !is_same<KW, K>::value, int> = 0>
auto KW_extract(K, T value, Ts... args){
	static_assert(0 != sizeof...(Ts), "Missing required parameter.");
	return KW_extract<P, KW>(args...);
}

// normal function definition
void f(char a, int x=2, double y=3.0){
	cout << a << x << y << "\n";
}

// the downside is that this:
//     template <typename T1, typename T2>
//     void f(T1 x, T2 y){...}
// needs a disable_if T1==KWARGS

// kwargs wrapper
template <typename... Ts>
auto f(KWARGS, Ts... args){
	return f(
		KW_extract<POS<0>, KW_a>(args...),
		KW_extract<POS<1>, KW_x>(args..., KW_x(), 2), // provide default value
		KW_extract<POS<2>, KW_y>(args..., KW_y(), 3.0)
	);
}

// alias
template <typename... Ts>
auto g(Ts... args){
	return f(args...);
}

// herecy alias
#define h g

int main(){
	f('1', 2, 3.4);
	f(KWARGS(), POS<0>(), '1', KW_y(), 3.4);
	g(KWARGS(), POS<0>(), '1', KW_y(), 3.4);
	h(KWARGS(), POS<0>(), '1', KW_y(), 3.4);

	return 0;
}
