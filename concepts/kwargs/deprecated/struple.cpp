
#include <iostream>
#include <tuple>

using namespace std;

class struple{
public:
	int& a;
	int& b;

	tuple<int, int> data;

	struple(tuple<int, int> data_):
		data{data_},
		a{get<0>(data)},
		b{get<1>(data)}
	{}
};

template <int I>
auto& get(struple& strup){
	return get<I>(strup.data);
}


int main(void){

	struple s(std::make_tuple(3,5));

	s.a = 7;

	auto tu = make_tuple(1,2);

	get<0>(s) = 8;
	cout << get<0>(s);

	cout << "awa\n";
	return 0;
}
