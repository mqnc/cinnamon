
#include <iostream>
#include <string>
#include <vector>
#include <tuple>

using namespace std;


// for string tags, will hopefully be replaced by template<string S> in c++20
template <char... Cs>
struct Name{};


// tagged value
template <typename TTag, typename Val>
class Tagged{
public:
	typedef TTag Tag;
	Val value;
};

// convenience function to create a tagged value
template <typename Tag, typename Val>
auto tag(Val value){
	return Tagged<Tag, Val>{value};
}

// for checking if an object is a tagged value
template<typename T>
struct is_tagged: public std::false_type {};

template<typename Tag, typename Val>
struct is_tagged<Tagged<Tag, Val> > : public std::true_type {};

template<typename T>
inline constexpr bool is_tagged_v = is_tagged<T>::value;

// can be fed to cout
template<typename Tag, typename Val>
ostream& operator<<(ostream& os, Tagged<Tag, Val> t){
	os << "<" << t.value << ">";
	return os;
}


// pair to cout
template<typename T1, typename T2>
ostream& operator<<(ostream& os, pair<T1, T2> arg){
	os << "[ " << arg.first << " | " << arg.second << " ]";
	return os;
}

// tuple to cout
template<typename... Ts>
ostream& operator<<(ostream& os, tuple<Ts...> tup){
	os << "( ";
	apply(
		[&](auto... arg){
			(os << ... << arg);
		}, tup
	);
	os << ")";
	return os;
}


void print(){}

template<typename T, typename... Ts>
void print(T arg, Ts... args){
	cout << arg;
	print(args...);
}


// very handy empty class
// TODO: this type must be hidden, it may not be used from the outside on purpose
struct Empty{
	const string value = "(empty)"; // for test purposes
};

// can be fed to cout
ostream& operator<<(ostream& os, Empty e){
	os << e.value;
	return os;
}



// argument list empty, return default value
template <size_t Idx, typename Tag, typename D>
auto extract(D dflt){
	return make_pair(
		dflt,
		tuple{}
	);
}

// returns a pair containing the find and a tuple of the rest
template <size_t Idx, typename Tag, typename D, typename T, typename... Ts>
auto extract(D dflt, T arg, Ts... args){

	// this lambda takes a result pair from a deeper recursion step
	// and appends the currently checked argument to the rest tuple
	// in the second field
	auto pass_on = [&](auto finding){
		return make_pair(
			finding.first,
			tuple_cat(make_tuple(arg), finding.second)
		);
	};

	// we haven't reached the tagged part of the list yet and we might still look for the positional argument
	if constexpr(!is_tagged_v<T> && Idx >= 0){
		// position fits -> return this argument
		if constexpr(Idx == 0){
			return make_pair(
				arg,
				make_tuple(args...)
			);
		}
		// position not reached yet -> move on
		else{
			return pass_on(extract<Idx-1, Tag>(dflt, args...));
		}
	}
	// we have reached the tagged part of the list, now only check the tag
	else{
		// tag matches -> return this argument
		if constexpr(is_same_v<Tag, typename T::Tag>){
			return make_pair(
				arg,
				make_tuple(args...)
			);
		}
		// key does not match, move on
		else{
			return pass_on(extract<-1, Tag>(dflt, args...));
		}
	}
}


template <size_t Idx, typename Tag, typename D, typename Lists>
auto filter(D dflt, Lists lists){
	auto done = lists.first;
	auto todo = lists.second;

	auto result = apply([&](auto... args){
		return extract<Idx, Tag>(dflt, args...);
	}, todo);

	auto find = result.first;
	auto rest = result.second;

	return make_pair(
		tuple_cat(make_tuple(find), done),
		rest
	);
}

// f(a0, a1="a1", a2, rest*)
template <typename... Ts>
void f(Ts... args){

	//cout << a0 << a1 << a2 << a3;
}


int main(int argc, char const *argv[]) {
	vector<string> args(argv, argv + argc);

	// todo: all assertions, maybe make the iterative parts in filter functional for Joachim
	// the thing below should not return wurst in the done part
	// ah it's because the tagged values are extracted first... crap...

	auto argus = make_tuple("pos0", "pos1", /*tag<Name<'t','2'>>("tag2"),*/ tag<Name<'t','3'>>("tag3"), "wurst");
	auto todo = make_pair(make_tuple(), argus);

	cout <<
	filter<0, Name<'t','0'> >(Empty(),
	 filter<1, Name<'t','1'> >(Empty(),
	  filter<2, Name<'t','2'> >(Empty(),
	   filter<3, Name<'t','3'> >(Empty(),
	    todo
	   )
	  )
	 )
	);

	/*f(
		"a0",
		"A1",
		tag<Name<'a', '3'> >("a3"),
		tag<Name<'a', '2'> >("a2")
	);*/

	return 0;
}
