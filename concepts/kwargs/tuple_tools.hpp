
#ifndef TUPLE_TOOLS_HPP
#define TUPLE_TOOLS_HPP

#include <iostream>
#include <tuple>

using namespace std;

// pair to cout
template<typename T1, typename T2>
ostream& operator<<(ostream& os, pair<T1, T2> arg){
	os << "[ " << arg.first << " | " << arg.second << " ]";
	return os;
}

// tuple to cout
// https://stackoverflow.com/questions/6245735/pretty-print-stdtuple
template<typename... Ts>
ostream& operator<<(ostream& os, tuple<Ts...> arg){
	auto print = [&](auto&& x) { os << x << " ";};
	os << "( ";
	std::apply([&](auto& ...x){(..., print(x));}, arg);
	os << ")";
	return os;
}



template <typename Tup>
auto tuple_first(Tup tup){
	return get<0>(tup);
}

template <typename T, typename Tup>
auto tuple_prepend(T arg, Tup tup){
	return tuple_cat(make_tuple(arg), tup);
}

template <typename Tup>
auto tuple_rmfirst(Tup tup){
	return apply(
		[](auto head, auto... tail){
			return make_tuple(tail...);
		}, tup
	);
}



template <typename Tup>
auto tuple_last(Tup tup){
	return get<tuple_size_v<Tup>-1>(tup);
}

template <typename Tup, typename T>
auto tuple_append(Tup tup, T arg){
	return tuple_cat(tup, make_tuple(arg));
}

template <typename Tup>
auto tuple_rmlast(Tup tup){
	if constexpr(tuple_size_v<Tup> <= 1){
		return make_tuple();
	}
	else{
		return tuple_prepend(
			tuple_first(tup),
			tuple_rmlast(
				tuple_rmfirst(tup)
			)
		);
	}
}


// this implementation is probably horrible but it will do for a prototype
template <size_t first, size_t after, typename Tup>
auto sub_tuple(Tup tup){
	if constexpr(first >= after){
		return make_tuple();
	}
	else if constexpr(first > 0){
		return sub_tuple<first-1, after-1>(tuple_rmfirst(tup));
	}
	else if constexpr(after < tuple_size_v<Tup>){
		return sub_tuple<0, after>(tuple_rmlast(tup));
	}
	else{
		return tup;
	}
}

template <int i, typename Tup, typename T>
auto tuple_insert(Tup tup, T arg){
	return tuple_cat(
		sub_tuple<0, i>(tup),
		make_tuple(arg),
		sub_tuple<i, tuple_size_v<Tup> >(tup)
	);
}

template <int i, typename Tup>
auto tuple_remove(Tup tup){
	return tuple_cat(
		sub_tuple<0, i>(tup),
		sub_tuple<i+1, tuple_size_v<Tup> >(tup)
	);
}

#endif

/*
int main(int argc, char const *argv[]) {

	cout << make_pair(1, 2) << '\n';

	auto t = make_tuple(0,1,2,3,4,5,6,7,8);

	cout << t << '\n';

	cout << tuple_first(t) << '\n';
	cout << tuple_prepend(-1, t) << '\n';
	cout << tuple_rmfirst(t) << '\n';

	cout << tuple_last(t) << '\n';
	cout << tuple_append(t, 9) << '\n';
	cout << tuple_rmlast(t) << '\n';

	cout << sub_tuple<3, 5>(t) << '\n';
	cout << tuple_insert<5>(t, 4.5) << '\n';
	cout << tuple_remove<5>(t) << '\n';

	return 0;
}
*/
