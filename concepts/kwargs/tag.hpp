
#ifndef TAG_HPP
#define TAG_HPP

#include <iostream>
#include <string>

using namespace std;

// for string tags, will hopefully be replaced by template<string S> in c++20
template <char... Cs>
struct Name{
	static const string str(){
		return (string("") + ... + Cs);
	}
};

// tagged value
template <typename TTag, typename Val>
class Tagged{
public:
	typedef TTag Tag;
	Val value;
};

// convenience function to create a tagged value
template <typename Tag, typename Val>
auto tag(Val value){
	return Tagged<Tag, Val>{value};
}

// for checking if an object is a tagged value
template<typename T>
struct is_tagged: public std::false_type {};

template<typename Tag, typename Val>
struct is_tagged<Tagged<Tag, Val> > : public std::true_type {};

template<typename T>
inline constexpr bool is_tagged_v = is_tagged<T>::value;

// can be fed to cout
template<typename Tag, typename Val>
ostream& operator<<(ostream& os, Tagged<Tag, Val> t){
	os << "<" << Tag::str() << ":" << t.value << ">";
	return os;
}

#endif

/*
int main(int argc, char const *argv[]) {

	typedef Name<'p','r','i','c','e'> Key;
	cout << Key::str() << '\n';
	auto t = tag<Key>(29.99);
	cout << t << '\n';
	cout << decltype(t)::Tag::str() << '\n';
	cout << t.value;

	return 0;
}
*/
