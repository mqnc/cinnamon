
#ifndef SUBSCRIPTOP_HPP
#define SUBSCRIPTOP_HPP

#ifndef STD_HPP
#include <memory>
#include <iostream>
#include <type_traits>
#endif

#include "canapply.hpp"

/*
handling all operator[] scenarios:

myObj[...]
calls invoke_subscript(myObj, ...) which calls:

if myObj[] then:
	*myObj if myObj supports operator*()
	myObj.invoke_subscript() otherwise

if myObj[arg] then:
	myObj[arg] if myObj supports operator[](arg)
	myObj.invoke_subscript(arg) otherwise

if myObj[arg1, arg2, ...] then:
	myObj.invoke_subscript(arg1, arg2, ...)

myObj[...] := myVal
is similar using assign_to_subscript(myVal, myObj, ...)
*/

template <class T>
using indirected_t = decltype( *(std::declval<T>()) );

template <class T>
using has_indirection = can_apply<indirected_t, T>;

template <class T, class Index>
using subscript_t = decltype(std::declval<T>()[std::declval<Index>()]);

template <class T, class Index>
using has_subscript = can_apply<subscript_t, T, Index>;


template <class T, std::enable_if_t<has_indirection<T>{}, int> = 0>
auto m0n_invoke_subscript(T& object){
	return *object;
}

template <class T, std::enable_if_t<!has_indirection<T>{}, int> = 0>
auto m0n_invoke_subscript(T& object){
	return object.m0n_invoke_subscript();
}

template <class T, class I, std::enable_if_t<has_subscript<T, I>{}, int> = 0>
auto m0n_invoke_subscript(T& object, I index){
	return object[index];
}

template <typename T, typename I, std::enable_if_t<!has_subscript<T, I>{}, int> = 0>
auto m0n_invoke_subscript(T& object, I index){
	return object.m0n_invoke_subscript(index);
}

template <typename T, typename ... Is>
auto m0n_invoke_subscript(T& object, Is ... indices){
	return object.m0n_invoke_subscript(indices...);
}


template <typename E, typename T, std::enable_if_t<has_indirection<T>{}, int> = 0>
void m0n_assign_to_subscript(E expr, T& object){
	*object = expr;
}

template <typename E, typename T, std::enable_if_t<!has_indirection<T>{}, int> = 0>
void m0n_assign_to_subscript(E expr, T& object){
	object.m0n_assign_to_subscript(expr);
}

template <typename E, typename T, typename I, std::enable_if_t<has_subscript<T, I>{}, int> = 0>
void m0n_assign_to_subscript(E expr, T& object, I index){
	object[index] = expr;
}

template <typename E, typename T, typename I, std::enable_if_t<!has_subscript<T, I>{}, int> = 0>
void m0n_assign_to_subscript(E expr, T& object, I index){
	object.m0n_assign_to_subscript(expr, index);
}

template <typename E, typename T, typename ... Is>
void m0n_assign_to_subscript(E expr, T& object, Is ... indices){
	object.m0n_assign_to_subscript(expr, indices...);
}

#endif
