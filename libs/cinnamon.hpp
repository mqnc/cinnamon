
#ifndef CINNAMON_HPP
#define CINNAMON_HPP

#ifndef STD_HPP
#include "std.hpp"
#endif

namespace cinnamon{

	#include "range.hpp"
	#include "print.hpp"
	#include "kwargs.hpp"
	#include "subscriptop.hpp"
	#include "smartpointers.hpp"

	const double inf = std::numeric_limits<double>::infinity();

	template <typename T>
	using rem_cv_ref = std::remove_cv_t<std::remove_reference_t<T> >;

	//#define rem_cv_ref<decltype(X) std::remove_cv_t<std::remove_reference_t<decltype(X)> >
	//#define addRefDecltype(X) std::add_lvalue_reference_t<decltype(X)>

	template <typename T>
	T& lval_ref_rval_save(T& range){return range;}

	template <typename T>
	T lval_ref_rval_save(T&& range){return range.save();}

	using Int = int;
	using Int8 = std::int8_t;
	using Int16 = std::int16_t;
	using Int32 = std::int32_t;
	using Int64 = std::int64_t;

	using UInt = unsigned int;
	using UInt8 = std::uint8_t;
	using UInt16 = std::uint16_t;
	using UInt32 = std::uint32_t;
	using UInt64 = std::uint64_t;

	using Float32 = float; //TODO
	using Float64 = double;

	using String = std::string;

	template<typename T>
	using DynArray = std::vector<T>;

	template<typename T, std::size_t N>
	using Array = std::array<T, N>;

}

#endif
