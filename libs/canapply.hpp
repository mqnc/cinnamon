
#ifndef CANAPPLY_HPP
#define CANAPPLY_HPP

// https://stackoverflow.com/a/31306194/3825996
// https://stackoverflow.com/a/40635163/3825996

namespace details {
	template<template<class...>class Z, class, class...Ts>
	struct can_apply: std::false_type{};
	template<template<class...>class Z, class...Ts>
	struct can_apply<Z, std::void_t<Z<Ts...>>, Ts...>: std::true_type{};
}

template<template<class...>class Z, class...Ts>
using can_apply=details::can_apply<Z,void,Ts...>;

#endif
