
#ifndef SMARTPOINTERS_HPP
#define SMARTPOINTERS_HPP

#include "stackbox.hpp"

#ifndef STD_HPP
#include <memory>
#endif

template <typename T>
using UniquePtr = std::unique_ptr<T>;

template <typename T>
using SharedPtr = std::shared_ptr<T>;

template <typename T>
using WeakPtr = std::weak_ptr<T>;

template <typename T>
auto make_weak(SharedPtr<T>& sp) -> WeakPtr<T>{
	return std::weak_ptr<T>(sp);
}

using std::make_unique;
using std::make_shared;
using std::move;

#endif
