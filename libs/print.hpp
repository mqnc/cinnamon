
#ifndef PRINT_HPP
#define PRINT_HPP

#ifndef STD_HPP
#include <iostream>
#endif

// feed no arguments to a stream
inline void m0n_feed(std::ostream& os){
	(void) os;
}

// feed one argument to a stream using <<
template<typename T, typename... Args>
void m0n_feed(std::ostream& os, const T &arg){
	os << arg;
}

// feed an arbitrary number of arguments to a stream
template<typename T, typename... Args>
void m0n_feed(std::ostream& os, const T &arg, const Args... args){
	m0n_feed(os, arg);
	m0n_feed(os, args...);
}

// fill a string
template<typename... Args>
std::string concat(const Args... args){
	std::stringstream ss;
	m0n_feed(ss, args...);
	return ss.str();
}

// print to cout
template<typename... Args>
void print(const Args... args){
	m0n_feed(std::cout, args...);
}

// print to cout and break line
template<typename... Args>
void println(const Args... args){
	print(args...);
	std::cout << std::endl;
}

#endif
