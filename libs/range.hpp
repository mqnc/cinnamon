
#ifndef RANGE_HPP
#define RANGE_HPP

// range prototype
template<typename T>
class Range{

private:
	class saveCtor{};

public:
	template<typename T1, typename T2, typename T3>
	Range(
		bool inclStart,
		const T1& start,
		const T2& incr,
		const T3& end,
		bool inclEnd
	):
		m_start(inclStart?start:start+incr),
		m_incr(incr),
		m_endidx((end-start)/incr-2 + inclStart + inclEnd),
		m_inf(false),
		m_idx(0)
	{}

	template<typename T1, typename T2>
	Range(
		bool inclStart,
		const T1& start,
		const T2& incr
	):
		m_start(inclStart?start:start+incr),
		m_incr(incr),
		m_endidx(0),
		m_inf(true),
		m_idx(0)
	{}
	Range(const Range<T>& other):
		m_start(other.m_start),
		m_incr(other.m_incr),
		m_endidx(other.m_endidx),
		m_inf(other.m_inf),
		m_idx(0)
	{}
	Range& operator=(const Range<T>& other){
		if (this != &other){
			m_start = other.m_start;
			m_incr = other.m_incr;
			m_endidx = other.m_endidx;
			m_inf = other.m_inf;
			m_idx = 0;
		}
		return *this;
	}
	T operator[](int idx) const{
		if(idx >= 0){
			assert((idx <= m_endidx) || m_inf);
			return m_start + m_incr*idx;
		}
		else{
			assert((idx >= -m_endidx-1) || m_inf);
			return m_start + m_incr*(m_endidx+1+idx);
		}
	}

	T front() const{
		assert(!empty());
		return m_start + m_incr*m_idx;
	}
	void popFront(){
		m_idx++;
	}
	bool empty() const{
		if(m_inf){return false;}
		return m_idx > m_endidx;
	}
	int size() const{
		if(!m_inf){
			return m_endidx + 1;
		}
		else{
			return std::numeric_limits<T>::max();
		}
	}
	Range save() const{
		return Range(saveCtor(), m_start, m_incr, m_endidx, m_inf, m_idx);
	}

private:
	Range(saveCtor, const T& start, const T& incr, const int& endidx, bool inf, const int& idx):
		m_start(start),
		m_incr(incr),
		m_endidx(endidx),
		m_inf(inf),
		m_idx(idx)
	{}

	T m_start;
	T m_incr;
	int m_endidx;
	bool m_inf;
	int m_idx;
};

// template deduction guides
template<typename T1, typename T2, typename T3>
Range(bool, const T1& s, const T2& i, const T3& e, bool) -> Range<decltype(s+i+e)>;

template<typename T1, typename T2>
Range(bool, const T1& s, const T2& i) -> Range<decltype(s+i)>;

#endif
