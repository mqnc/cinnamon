


we create a common interface for the special construction things:

cinnamon -> cpp

frob() -> frob()
frob[] -> frob(LBRACKET, RBRACKET)
frob(] -> frob(RBRACKET)
frob(5) -> frob(5)
frob[5] -> frob(LBRACKET, 5, RBRACKET)
Range[1..2..3] -> Range(LBRACKET, 1, DDOT, 2, DDOT, 3, RBRACKET)
Matrix[1, 2; 3, 4] -> Matrix(LBRACKET, 1, 2, BREAK, 3, 4, RBRACKET)
Array[[1,2], [3,4]] -> Array(LBRACKET, Tuple(LBRACKET, 1, 2, RBRACKET), Tuple(LBRACKET, 3, 4, RBRACKET), RBRACKET)
Vector[[1,2], [3,4]] -> Vector(LBRACKET, Tuple(LBRACKET, 1, 2, RBRACKET), Tuple(LBRACKET, 3, 4, RBRACKET), RBRACKET)

And then the constructors automatically turn tuples into what they are themselves, so

Array[[1,2], [3,4]] becomes Array[Array[1,2], Array[3,4]]
Vector[[1,2], [3,4]] becomes Vector[Vector[1,2], Vector[3,4]]

but you can also manually do

Vector[Array[1,2], Array[3,4]]
