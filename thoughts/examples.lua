
buffer = {}
function out(txt)
	table.insert(buffer, txt)
end

function markdown(title, cin, cpp, cinTitle, cppTitle)
	if cinTitle == nil then
		cinTitle = "Cinnamon"
	end

	if cppTitle == nil then
		cppTitle = "C++"
	end

	out([[
<tr><td colspan=2>&nbsp;<h4>]] .. title .. [[</h4></td></tr>
<tr><td>

]] .. cinTitle .. "\n")

	for i=1, #cin do
		out([[

```cpp
]] .. cin[i] .. [[
```

]])
	end

	out([[
</td><td>

]] .. cppTitle .. "\n")

	for i=1, #cpp do
		out([[

```cpp
]] .. cpp[i] .. [[
```

]])
	end

	out([[</td></tr>
]])

end

out("<table>\n")

markdown("Hello World!", {[[
function main
	println("Hello World!")
end
]]}, {[[
#include <iostream>

int main(){
	std::cout << "Hello World!\n";
}
]]})

markdown("Templated arguments and multiple return values", {[[
function swap(x, y)
	return y, x
end
]]}, {[[
template <typename T1, typename T2>
auto swap(const T1 x, const T2 y){
	return std::make_tuple(y, x);
}
]]})

markdown("Named return values", {[[
function divide(a:Int, b:Int) -> q:Int, r:Int
	return a div b, a mod b
end
]]}, {[[
struct DivisionResult{
	int q;
	int r;
};

DivisionResult divide(int a, int b){
	return {a / b, a % b};
}
]]})

markdown("Keyword arguments", {[[
function drawLine(...
		p0:Point, ...
		p1:Point, ...
		width := 1, ...
		color := BLACK, ...
		dashed := false, ...
		caps := SQUARE, ...
		join := BEVEL) | kwargs

	// do stuff using p0, p1 and so on

end

// call:
drawLine(p0, p1, caps := ROUND, join := ROUND)
]]}, {[[
class LineParams{

	friend void drawLine(LineParams p);

public:

	LineParams(Point p0, Point p1):
		required_p0_m(p0),
		required_p1_m(p1),
		optional_width_m(1),
		optional_color_m(BLACK),
		optional_dashed_m(false),
		optional_caps_m(SQUARE),
		optional_join_m(BEVEL){}

	LineParams& width(int w){
		optional_width_m = w;
		return *this;
	}

	LineParams& color(Color c){
		optional_color_m = c;
		return *this;
	}

	LineParams& dashed(bool d){
		optional_dashed_m = d;
		return *this;
	}

	LineParams& caps(Caps c){
		optional_caps_m = c;
		return *this;
	}

	LineParams& join(Join j){
		optional_join_m = j;
		return *this;
	}

private:
	Point required_p0_m;
	Point required_p1_m;
	int optional_width_m;
	Color optional_color_m;
	bool optional_dashed_m;
	Caps optional_caps_m;
	Join optional_join_m;
};

void drawLine(LineParams p){

	// do stuff using p.p0, p.p1 and so on

}

// call:
drawLine(LineParams(p0, p1).caps(ROUND).join(ROUND))
]]}, nil, "C++ (from [rosettacode](https://rosettacode.org/wiki/Named_parameters#C.2B.2B))")

markdown("Ranges and execution between iterations", {[[
// 1 2 3 4 5 6 7 8 9
for var i in [1..9]
	print(i, " ")
end

// 1 2 3 4 5 6 7 8 9
for var i in (0..10)
	print(i, " ")
end

// 1 2 3 4 5 6 7 8 9
for var i in [1..10)
	print(i, " ")
end

// 2, 4, 6, 8
for var i in (0..2..10)
	print(i)
between
	print(", ")
end
]]}, {[[
// 1 2 3 4 5 6 7 8 9
for(int i=1; i<=9; i++){
	std::cout << i << " ";
}

// 1 2 3 4 5 6 7 8 9
for(int i=1; i<10; i++){
	std::cout << i << " ";
}

// 1 2 3 4 5 6 7 8 9
for(int i=1; i<10; i++){
	std::cout << i << " ";
}

// 2, 4, 6, 8
bool first = true;
for(int i=2; i<10; i+=2){
	std::cout << i;
	if(!first){
		std::cout << ", ";
		first = false;
	}
}
]]}, "Cinnamon<br>(implementing [Alexandrescu's philosophy](http://www.informit.com/articles/printerfriendly/1407357))")

markdown("Classes", {[[
class SpaceShip inherits Object

	const id := 0
	var shield := 0
	var ammo := 0
	(var) secretMission := "" // private
	[var] pilot := "" // protected

	ctor(id:Int, shield:Int, ammo:Int, ...
			mission:String, pilot:String)
		init
			self.id := id
			self.shield := shield
			self.ammo := ammo
			self.secretMission := ...
					self.decrypt(mission)
			self.pilot := pilot
		end

		// stuff
	end

	// private method
	(method) decrypt(text:String)
		// stuff
	end

	// public method
	method update()
		// stuff
	end

	dtor
		// cleanup stuff
	end
end
]]}, {[[
// forward declaration
class SpaceShip;

class SpaceShip:public Object
{
public:
	SpaceShip(
		const int id,
		const int shield,
		const int ammo,
		const std::string mission,
		const std::string pilot
	);
	void update();
	~SpaceShip();
private:
	void decrypt(const String text);

public:
	const int id = 0;
	int shield = 0;
	int ammo = 0;
private:
	std::string secretMission = "";
protected:
	std::string pilot = "";
};

SpaceShip::SpaceShip(
	const int id,
	const int shield,
	const int ammo,
	const std::string mission,
	const std::string pilot
):
	id{id},
	shield{shield},
	ammo{ammo},
	secretMission{
		decrypt(mission)
	},
	pilot{pilot}
{
	// stuff
}

void SpaceShip::decrypt(const String text)
{
	// stuff
}

void SpaceShip::update()
{
	// stuff
}

SpaceShip::~SpaceShip()
{
	// cleanup stuff
}
]]})

markdown("Modules", {[[
// lib.mon

function interface(i:Int, s:String) | export
	implementation()
end

function implementation()
	// stuff
end
]], [[
// main.mon

import lib
from otherlib import frobnicate as frob
from dirtylib import *

function main()
	lib::interface(1337, "covfefe")
	frob()
	hackery() // from dirtylib
end
]]}, {[[
// lib.hpp

#ifndef LIB_HPP
#define LIB_HPP

#include <string>

namespace lib{
	void interface(int i, std::string s);
}
#endif
]], [[
// lib.cpp

#include "lib.hpp"

namespace lib{
	void implementation();

	void interface(int i, std::string s){
		implementation();
	}

	void implementation(){
		// stuff
	}
}
]], [[
// main.cpp

#include "lib.hpp"
#include "otherlib.hpp"
#include "dirtylib.hpp"

using frob = otherlib::frobnicate;
using namespace dirtylib;

int main(){
	lib::interface(1337, "covfefe");
	frob();
	hackery(); // from dirtylib
	return 0;
}
]]})

markdown("The little things", {[[
class MyClass

	// multiple parameters for subscript
	method[param1, param2]
		// stuff
	end

end // no semicolon!!!

// main arguments are a std::vector
function main(args:DynArray)

	// there is no single "="
	var ready := power == 9000

	// bitwise operators have higher
	// precedence than comparisons
	if flags bitand mask == 0
		// stuff
	end

	// everything is always RAIId
	// and auto-typed (parameters can
	// have explicit type tho)
	var myObject := MyClass()

	// ";" and linebreak are equivalent
	var px := 1.4; var py := 3.1

	// tuples like in python
	var a, b := 7, 2
	var ab := a, b
	a, b := ab

	switch phase
		case PHASE1, PHASE2, PHASE3
			// break is implicit
		case CLEANUP
			// fallthrough is explicit
			fall
		case EXIT
			// exit stuff
		case default
			// default stuff
	end

	// custom infix operators
	// with custom precedence
	// via plugins
	var volume := a · b × c

	// unicode identifiers
	var ポケモン := "ピカチュウ"

end
]]}, {[=[
// MyClass.hpp

class MyClass{

	void operator[](std::tuple<int, int> params){
		// stuff
	}

} // uh oh, forgot the semicolon
// compilers be like "something completely different
// is wrong in a completely unrelated file!"
]=], [=[
// main.cpp

// can't believe how c this is
int main(int argc, char *argv[]){

	if(power = 9000) // well I'm sure it is now
	{}

	if(flags & mask == 0){ // uh oh
		// this was (flags & (mask == 0))
	}

	MyClass myObject(); // uh oh
	// accidentally declared a function here

	auto px = 1.4, py = 3.1;
	// well that's actually ok

	auto [a, b] = std::make_tuple(7, 2);
	auto ab = std::make_tuple(a, b);
	std::tie(a, b) = ab;

	switch phase{
		case PHASE1: case PHASE2: case PHASE3:
			// uh oh, forgot the break
		case CLEANUP:
			// get a compiler warning
			// if I don't put [[fallthrough]]
		case EXIT:
			// exit stuff
		default:
			// default stuff
	end

	double volume = a.dot(b.cross(c));

	auto xenophobicLatinVarname = "ピカチュウ";
}
]=]})

markdown("Anything you <i>really</i> like to do in C++", {[[
C++{goto batcave;}
]]}, {[[
goto batcave;
]]})


out("</table>\n")

file = io.open("../build/examples.md", "w")
io.output(file)
io.write((table.concat(buffer):gsub("\t", "    ")))
io.close(file)
