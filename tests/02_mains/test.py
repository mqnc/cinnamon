
env.transpile("void_main_void.mon", findInCpp = ["return 0;"])
env.compile()
env.run()

env.line()

env.transpile("int_main_void.mon")
env.compile()
env.run(expectedReturn = 133)

env.line()

env.transpile("auto_main_void.mon")
env.compile()
env.run(expectedReturn = 134)

env.line()

env.transpile("auto_main_args.mon")
env.compile()
env.run(arguments = "narf troz", expectedOutputs = ["narf", "troz"], expectedReturn = 3)
