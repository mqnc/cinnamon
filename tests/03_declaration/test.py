
env.transpile("work.mon", findInCpp = ["assign_to_subscript", "invoke_subscript"])
env.compile()
env.run()

env.transpile("fail.mon")
env.compile(expectedErrors = [
		"assignment of read-only variable 'i'",
		"assignment of read-only reference 'b'"
])
