
env.transpile("work.mon")
env.compile()
env.run()

env.transpile("fail.mon")
env.compile(expectedErrors = [
	"21:24: error: 'module2_m0n_ns::fsecret' has not been declared",
	"32:26: error: call of overloaded 'fconflict()' is ambiguous",
	"33:16: error: 'f1' is not a member of 'fail_m0n_ns'",
	"34:16: error: 'fail_m0n_ns::module2' has not been declared",
	"35:16: error: 'fail_m0n_ns::module3' has not been declared",
	"36:12: error: 'f4' is not a member of 'fail_m0n_ns::module4'",
	"37:16: error: 'fail_m0n_ns::submodule4' has not been declared",
	"38:16: error: 'f4' is not a member of 'fail_m0n_ns'",
	"39:12: error: 'fail_m0n_ns::module5::submodule5' has not been declared",
	"40:16: error: 'fail_m0n_ns::submodule5' has not been declared",
	"41:16: error: 'f5' is not a member of 'fail_m0n_ns'",
	"42:12: error: 'fail_m0n_ns::module5::submodule5' has not been declared",
	"43:12: error: 'fprivate5' is not a member of 'fail_m0n_ns::module5'",
	"44:12: error: 'fail_m0n_ns::module6::submodule6' has not been declared",
	"45:16: error: 'fail_m0n_ns::submodule6' has not been declared",
	"46:16: error: 'f6' is not a member of 'fail_m0n_ns'",
	"47:16: error: 'fail_m0n_ns::module7' has not been declared",
	"48:16: error: 'fail_m0n_ns::module7' has not been declared",
	"49:16: error: 'fail_m0n_ns::submodule7' has not been declared",
	"50:16: error: 'fail_m0n_ns::module8' has not been declared",
	"51:16: error: 'fail_m0n_ns::module8' has not been declared",
	"52:16: error: 'fail_m0n_ns::submodule8' has not been declared",
	"53:16: error: 'fail_m0n_ns::module9' has not been declared",
	"54:16: error: 'fail_m0n_ns::module9' has not been declared",
	"55:16: error: 'f9' is not a member of 'fail_m0n_ns'",
	"56:25: error: assignment of read-only variable 'common_m0n_ns::globalConst'",
	"57:51: error: assignment of read-only variable 'common_m0n_ns::globalTemplateConst<int>'",
	"58:11: error: 'localVar' is not a member of 'fail_m0n_ns::common'",
	"59:11: error: 'localConst' is not a member of 'fail_m0n_ns::common'",
	"60:11: error: 'localTemplateVar' is not a member of 'fail_m0n_ns::common'",
	"61:11: error: 'localTemplateConst' is not a member of 'fail_m0n_ns::common'"
])
