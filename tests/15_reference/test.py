env.transpile("work.mon")
env.compile()
env.run()

env.transpile("fail.mon")
env.compile(expectedErrors = [
		"error: assignment of read-only reference 'i'",
		"error: assignment of read-only reference 'j'"
])
