
env.transpile("main.mon", findInCpp = [
	"class Demo:public Fields, private Empty, protected Jazz",
	"public: rem_cv_ref<decltype(1.0)> x = 1.0;",
	"public: const rem_cv_ref<decltype(2)> i = 2;",
	'private: rem_cv_ref<decltype(std::string("string"))> s = std::string("string");',
	"protected: const rem_cv_ref<decltype('q')> q = 'q';",
	"private: inline void doStuff() const;",
	"protected: void doOtherStuff();",
	"public: void operator()();"
])
env.compile()
env.run(expectedOutputs = [
	"()",
	"[]:=17",
	"[1]",
	"[12]",
	"[1234]:=12.34cm",
	"abababab",
	"41",
	"43",
	"R2D2?!"
])
