
env.transpile("main.mon", findInCpp = [
		"void nothing()",
		"void nothing_with_parens()",
		"one_param(Int",
		"one_const_param(const Int",
		"template_var(m0n_Tt",
		"template_const(const m0n_Tt",
		"void void_return()",
		"auto return_type() -> main_m0n_ns::Int",
		"auto return_tuple() -> std::tuple",
		"auto return_struct() -> return_struct_m0n_result",
		"inline static void inlinefn();",
		"kwargmap", "kwargwrap",
		"inline auto specs_m0n_kwargwrap"
])
env.compile()
env.run(expectedOutputs = ["0123456789abcdefg999hi999"])
