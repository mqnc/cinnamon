
realProblems = 0

def assertProblemsAndReset(expected):
	global env, realProblems
	if env.problems != expected:
		print("\n/|\\ Expected number of problems: " + str(expected))
		print("\\o/ Actual number of problems: " + str(env.problems))
		realProblems = realProblems + 1
	env.problems = 0


env.transpile("fail_transpile.mon")
assertProblemsAndReset(2)
env.transpile("fail_transpile.mon",
		expectedErrors = ["(not occuring transpile error)"],
		findInHeader = ["(not in header)"],
		findInCpp = ["(not in cpp)"])
assertProblemsAndReset(4)

env.line()

env.transpile("fail_compile.mon")
env.compile()
assertProblemsAndReset(1)
env.compile(expectedErrors = ["(not occuring compile error)"])
assertProblemsAndReset(2)

env.line()

env.transpile("fail_run.mon")
env.compile()
env.run()
assertProblemsAndReset(1)
env.run(arguments = "",
		expectedOutputs = ["(not in output)"],
		unwantedOutputs = ["fail"],
		expectedReturn = 0)
assertProblemsAndReset(3)

print("Problems until this point were intentional")
env.problems = realProblems

env.line()

env.transpile("fail_transpile.mon",
		expectedErrors = ["Invalid Syntax"],
		findInHeader = ["cinnamon.hpp"],
		findInCpp = ["fail_transpile.hpp"])

env.line()

env.transpile("fail_compile.mon")
env.compile(expectedErrors = ["error: 'undeclared_identifier' is not a member of 'fail_compile_m0n_ns'"])

env.line()

env.transpile("fail_run.mon")
env.compile()
env.run(arguments = "",
		expectedOutputs = ["fail"],
		unwantedOutputs = ["(not in output)"],
		expectedReturn = None)
